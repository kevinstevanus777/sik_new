﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterHomepage.Master" AutoEventWireup="true" CodeBehind="ProposalInputMhs.aspx.cs" Inherits="SIK_NEW.ProposalInputMhs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="TopPage" runat="server">
    <h1>Input Proposal</h1>
    <form runat="server">

        <div class="col-md-5" id="divInputProker" runat="server" visible="true">
            Kode Program Kerja
            <br />
            <asp:DropDownList ID="ikodeProker" runat="server" DataSourceID="sqlProgramKerja" DataTextField="JUDUL_KEGIATAN" DataValueField="KODE_PROGRAM" CssClass="form-control" AutoPostBack="false">
            </asp:DropDownList>
            <asp:SqlDataSource ID="sqlProgramKerja" runat="server" ConnectionString="<%$ ConnectionStrings:SIKConnectionString %>" SelectCommand="SELECT PK.JUDUL_KEGIATAN, PK.KODE_PROGRAM
FROM PROGRAM_KERJA PK LEFT JOIN  PROPOSAL P ON P.KODE_PROGRAM = PK.KODE_PROGRAM
LEFT JOIN ORGANISASI_PERIODE OP ON OP.KODE_ORGANISASI = PK.KODE_ORGANISASI AND OP.PERIODE = PK.PERIODE
LEFT JOIN PIC PIC ON PIC.PERIODE = OP.PERIODE AND PIC.KODE_ORGANISASI = PIC.KODE_ORGANISASI
WHERE NO_SURAT_PROPOSAL IS NULL AND PIC.NIM = @NIM AND PK.STATUS_PROKER = 1 AND PK.FISIK_IS_RECEIVED = 1">
                <SelectParameters>
                    <asp:SessionParameter Name="NIM" SessionField="NIM" />
                </SelectParameters>
            </asp:SqlDataSource>

            <%--Penambahan --%>



            <%--  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link rel="stylesheet" href="/resources/demos/style.css">
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>--%>
            <div class="form-group">
                <input id="iDateFrom" name="iDateFrom[]" type="text" clientidmode="Static" />
                <input id="iDateFinish" name="iDateFinish[]" type="text" clientidmode="Static" />
                <%--js untuk datetimepicker--%>
                <button id="btAddTanggalProposal"  type="button">Tambah Tanggal</button>
            </div>

            <div id="inputDateDiv">
            </div>



            <div class="form-group">
                <label for="exampleInputpj">Penanggung Jawab</label>
                <input type="text" class="form-control" id="iPenanggungJawab" runat="server" aria-describedby="emailHelp" placeholder="Input Penanggung Jawab">
            </div>

            <div class="form-group">
                <label for="Dana">Dana Bantuan</label>
                <input type="number" min="0" class="form-control" id="iDana" runat="server" aria-describedby="emailHelp" placeholder="Input Dana">
            </div>

            <div class="form-group">
                <label for="exampleInputth">Tahun</label>
                <input type="text" class="form-control" id="iTahun" runat="server" aria-describedby="emailHelp" placeholder="Input Tahun">
            </div>

            <div class="form-group">
                <label for="exampleInputtmpt">Tempat Pelaksanaan </label>
                <input type="text" class="form-control" id="iTempatPelaksanaan" runat="server" aria-describedby="emailHelp" placeholder="Input Tempat Pelaksanaan Kegiatan">
            </div>

            <div class="form-group">
                <label for="exampleInputjdl">Judul Kegiatan </label>
                <input type="text" class="form-control" id="iJudulKegiatan" runat="server" aria-describedby="emailHelp" placeholder="Input Judul Kegiatan">
            </div>

            <div class="form-group">
                <label for="file">Upload File PDF </label>
                <div class="col-sm-6">
                    <asp:FileUpload ID="PDFUploadProker" runat="server" />
                    <br />
                    <asp:Label ID="LblMessage" runat="server" Text="Label"></asp:Label>

                </div>
            </div>

            <asp:Button ID="btSubmitProposal" CssClass="btn btn-primary" runat="server" Text="Submit" OnClick="btSubmitProposal_Click" />
            <asp:SqlDataSource ID="SqlProposal" runat="server" ConnectionString='<%$ ConnectionStrings:SIKConnectionString %>' SelectCommand="
                        SELECT *FROM PROPOSAL "></asp:SqlDataSource>
            <br />
            <asp:Button ID="btSubmitNameMhs" CssClass="btn btn-primary" runat="server" Text="Add Nama Peserta" OnClick="btSubmitNameMhs_Click" />
            <br />
            <br />

            <div class="form-group">
                <label for="exampleInputjdl">Nim Para Peserta </label>
                <input type="text" class="form-control" id="iNIMPeserta" runat="server" aria-describedby="emailHelp" placeholder="NIM Peserta">
            </div>

            <br />
            <asp:Button ID="Button1" runat="server" OnClick="Button1_Click" Text="Button" />
            <br />
            <br />
        </div>


        <asp:GridView ID="GvProposal" runat="server" DataKeyNames="NO_SURAT_PROPOSAL" AllowSorting="true" DataSourceID="SqlProposal" AutoGenerateColumns="true" Visible="false">
        </asp:GridView>
        <%--ari--%>
        <div id="divAddPeserta" runat="server" visible="false">
            <div class="form-group">
                <label for="exampleInputEmail1">Nama Peserta</label>
                <input type="text" class="form-control" id="inamapeserta" runat="server" aria-describedby="emailHelp" placeholder="NamePeserta">
            </div>
            <asp:Button ID="SubmitPeserta" CssClass="btn btn-primary" runat="server" Text="Submit Peserta" OnClick="SubmitPeserta_Click" />
        </div>











    </form>




    <!-- js untuk datepicker -->
    <%--<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>--%>
    <%--    <script>
        //di proposal datepicker 
        $(function () {
            $("#iTanggalTerima").datepicker();
        });


    </script>--%>
    <script src="/js/jquery.js"></script>
    <script src="/js/jquery.datetimepicker.full.min.js"></script>

    <%--ari--%>
    <%--<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>--%>


    <script>
        //yang ini bikin jadi di tanggal 20 agustus 2020
        //jQuery("#iDateFrom").datetimepicker({ minDate: 0 ,startDate: '2020/08/20'});
        //https://stackoverflow.com/questions/4419804/restrict-date-in-jquery-datepicker-based-on-another-datepicker-or-textbox
        //defaultDate untuk pindahin biar ke bulan selanjutnya , caranya dengan menambahkan 1970/01/X where X = tanggalnya berapa, 1970/01/02 adalah tommorrow
        jQuery("#iDateFrom").datetimepicker({
            defaultDate: '+1970/01/31',
            minDate: '+1970/01/31',
            format: 'Y/m/d H:i',

            changeMonth: true,
            onSelectDate: function (date) {
                var selectedDate = new Date(date);
                var msecsInADay = 86400000;

                var foo = new Date(date);
                console.log("FOO: " + foo.getDate() + "/" + foo.getMonth() + "/" + foo.getFullYear());

                var endDate = new Date(selectedDate.getTime().msecsInADay);
                console.log(endDate);
                console.log(selectedDate);
                $('#iDateFinish').datetimepicker("option", "minDate", foo);
                $('#iDateFinish').datetimepicker("option", "defaultDate", foo);


                //ini bisa padahal
                //$('#iDateFinish').datetimepicker("option", "minDate", "+1970/02/07");
                //$('#iDateFinish').datetimepicker("option","defaultDate","+1970/02/07");

            }

        });


    </script>
    <script>
        jQuery("#iDateFinish").datetimepicker({ format: 'Y/m/d H:i' });
        //jQuery("#iDateFinish").datetimepicker({ defaultDate: '+1970/01/31',minDate: '+1970/01/31'});
    </script>
    <script>
        jQuery("#iTestInput").datetimepicker();
    </script>

    
<%--    <script>
        $("#btAddTanggalProposal").click(function () {

            var html = '';


            html += '<div class="form-group">';
            html += '<label for="exampleInputEmail1">Email address</label>';
            html += '<input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email">';
            html += '<small id="emailHelp" class="form-text text-muted">Well never share your email with anyone else.</small>';
            html += '</div>';



            $('#inputDateDiv').append(html);

        });
    </script>--%>


    <script>
        $("#btAddTanggalProposal").click(function () {

            var html = '';


            html += '<div class="form-group">';
            html += '<input name="iDateFrom[]" type="text" class="iDateFrom" clientidmode="Static" />';
            html += '<input name="iDateFinish[]" type="text" class="iDateFrom" clientidmode="Static" />';
            html += '</div>';



            $('#inputDateDiv').append(html);




            jQuery(".iDateFrom").datetimepicker({
                defaultDate: '+1970/01/31',
                minDate: '+1970/01/31',
                format: 'Y/m/d H:i',

                changeMonth: true,
                onSelectDate: function (date) {
                    var selectedDate = new Date(date);
                    var msecsInADay = 86400000;

                    var foo = new Date(date);
                    console.log("FOO: " + foo.getDate() + "/" + foo.getMonth() + "/" + foo.getFullYear());

                    var endDate = new Date(selectedDate.getTime().msecsInADay);
                    console.log(endDate);
                    console.log(selectedDate);
                    $('#iDateFinish').datetimepicker("option", "minDate", foo);
                    $('#iDateFinish').datetimepicker("option", "defaultDate", foo);


                    //ini bisa padahal
                    //$('#iDateFinish').datetimepicker("option", "minDate", "+1970/02/07");
                    //$('#iDateFinish').datetimepicker("option","defaultDate","+1970/02/07");

                }

            });


            jQuery(".iDateFinish").datetimepicker({ format: 'Y/m/d H:i' });

        });
    </script>



    <!--js untuk proposal datepicker -->
    <%--    <script>
        //di proposal datepicker 
        $(function () {
            $("#iTanggalTerima").datepicker();
        });


    </script>--%>
</asp:Content>
