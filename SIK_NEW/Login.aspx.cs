﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SIK_NEW
{
    public partial class WebForm4 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {


        }

        protected void btLogin_Click(object sender, EventArgs e)
        {
           
          

            //jika box nama kosong
            if (iNama.Value == "" || iNama.Value == null)
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('Nama masih kosong')", true);

                return;
            }


            //cek apakah ada nik didalam database
            SIKTableAdapters.MAHASISWATableAdapter ta = new SIKTableAdapters.MAHASISWATableAdapter();
            SIK.MAHASISWADataTable dt = ta.SelectLoginMahasiswa(iNama.Value);

            //jika query mengembalikan row
            if (dt.Rows.Count > 0)
            {
                SIK.MAHASISWARow da = (SIK.MAHASISWARow)dt.Rows[0];
                Session["NIM"] = da.NIM;
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('Berhasil Login')", true);
            }
            //jika kosong
            else
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('Nama Tidak Ditemukan di database')", true);
                System.Diagnostics.Debug.WriteLine("kosong");
            }



        }

        protected void btSignUp_Click(object sender, EventArgs e)
        {
            //jika box nama kosong
            if (iNama.Value == "" || iNama.Value == null)
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('Nama masih kosong')", true);

                return;
            }
            else
            {
                //insert ke tabel
                SIKTableAdapters.MAHASISWATableAdapter ta = new SIKTableAdapters.MAHASISWATableAdapter();
                ta.InsertMahasiswa(iNama.Value);
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('Mahasiswa Berhasil Ditambahkan')", true);
                Response.Redirect(Request.RawUrl);

            }

        }
    }
}