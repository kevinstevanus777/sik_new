﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Management;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SIK_NEW
{
    public partial class ProposalInputMhs : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            if (Session["NIM"] == null)
            {
                Response.Redirect("Login.aspx");
            }


            /*
            SIKTableAdapters.PROGRAM_KERJATableAdapter taproker = new SIKTableAdapters.PROGRAM_KERJATableAdapter();
            SIK.PROGRAM_KERJADataTable daproker = taproker.SelectProkerMhs();

            DDProgramKerja.DataTextField = daproker.Columns["JUDUL_KEGIATAN"].ToString();
            DDProgramKerja.DataValueField = daproker.Columns["KODE_PROGRAM"].ToString();

            DDProgramKerja.DataSource = daproker;
            DDProgramKerja.DataBind();
            */


            //TextBoxDatetimefrom.Text = DateTime.Now.ToString("MM/dd/yyyy hh:mm tt");
            //TextBoxDatetimeto.Text = DateTime.Now.ToString("MM/dd/yyyy hh:mm tt");

        }

        protected void btSubmitProposal_Click(object sender, EventArgs e)
        {
            

            divAddPeserta.Visible = false;
            GvProposal.Visible = true;

            if (PDFUploadProker.HasFile)
            {
                System.Diagnostics.Debug.WriteLine("test");
                string extensionPDF = System.IO.Path.GetExtension(PDFUploadProker.FileName);
                if (extensionPDF.ToLower() == ".pdf")
                {
                    PDFUploadProker.SaveAs(MapPath("~/uploads/" + PDFUploadProker.FileName));
                    LblMessage.Text = "File succeed Uploaded";

                    //INSERT PROPOSAL
                    //var iDatetimeFrom = DateTime.Now;
                    //DateTime myDate = DateTime.ParseExact(TextBoxDatetimefrom.Text, "MM/dd/yyyy hh:mm tt", System.Globalization.CultureInfo.InvariantCulture);
                    // DateTime myDateto = DateTime.ParseExact(TextBoxDatetimeto.Text, "MM/dd/yyyy hh:mm tt", System.Globalization.CultureInfo.InvariantCulture);
                    // var mydatefrom = Convert.ToDateTime(TextBoxDatetimefrom.Text);
                    //  var mydateto = Convert.ToDateTime(TextBoxDatetimeto.Text);

                    // string startdatetime = TextBoxDatetimefrom.Text;
                    //string enddatetime = TextBoxDatetimeto.Text;



                    int danaBantuan = Convert.ToInt32(iDana.Value);
                    int tahun = Convert.ToInt32(iTahun.Value);
                    
                    DateTime dtime = Convert.ToDateTime(DateTime.Now, System.Globalization.CultureInfo.GetCultureInfo("id-ID").DateTimeFormat);

                    //string s = dtime.ToString("dd/mm/yyyy", CultureInfo.InvariantCulture);

                    SIKTableAdapters.PROPOSALTableAdapter taproposal = new SIKTableAdapters.PROPOSALTableAdapter();
                    // taproposal.InsertProposal(ikodeProker.SelectedValue, iTanggalTerima.Text, PDFUploadProker.FileName,iTahun.Value, iPenanggungJawab.Value, false ,iTempatPelaksanaan.Value, iJudulKegiatan.Value, iDana);
                    
                    //insert ke tabel proposal utama
                    taproposal.InsertProposal(ikodeProker.SelectedValue, PDFUploadProker.FileName, iPenanggungJawab.Value, false, danaBantuan, iTempatPelaksanaan.Value, iJudulKegiatan.Value,tahun, dtime.ToString());
                    System.Diagnostics.Debug.WriteLine("fired");


                    //ambil nomor proposal terakhir
                    SIK.PROPOSALDataTable dt =  taproposal.SelectLastKodeProposal();
                    SIK.PROPOSALRow da = (SIK.PROPOSALRow)dt.Rows[0];
                    string lastNum = da.NO_SURAT_PROPOSAL;


                    SIKTableAdapters.PROPOSAL_WAKTUTableAdapter tapowaktu = new SIKTableAdapters.PROPOSAL_WAKTUTableAdapter();

                    //insert waktu 
                    string[] valuesX = Request.Form.GetValues("iDateFrom[]");
                    string[] valuesY = Request.Form.GetValues("iDateFinish[]");


                    for (int i = 0; i < valuesX.Length; i++)
                    {
                        tapowaktu.InsertProposalWaktu(lastNum,DateTime.Parse(valuesX[i]),DateTime.Parse(valuesY[i]));


                    }


                    SIKTableAdapters.DETAIL_PROPOSALTableAdapter tad = new SIKTableAdapters.DETAIL_PROPOSALTableAdapter();

                    foreach (string s in _peserta)
                    {
                        tad.InsertDetailProp(lastNum, s);

                    }




                }
                else
                {

           
                    System.Diagnostics.Debug.WriteLine("not fired");
                    return;
                }

            }



        }

        

        protected void btSubmitNameMhs_Click(object sender, EventArgs e)
        {
         
            //divInputProker.Visible = false;
            //divAddPeserta.Visible = true;
            //GvProposal.Visible = false;
            
        }

        protected void SubmitPeserta_Click(object sender, EventArgs e)
        {
   
            SIKTableAdapters.MAHASISWATableAdapter tamhs = new SIKTableAdapters.MAHASISWATableAdapter();
            tamhs.InsertMahasiswa(inamapeserta.Value);
        }



        public class ListNIMPeserta
        {
            public string NIMPeserta { get; set; }
        }


        protected void btAddTanggalProposal_Click(object sender, EventArgs e)
        {


         

            System.Diagnostics.Debug.WriteLine(" ");

            //show date ke div barunya

        }

        public static List<string> _peserta = new List<string>();

        protected void Button1_Click(object sender, EventArgs e)
        {
            _peserta.Add(iNIMPeserta.Value);

            foreach (string s in _peserta)
            {
                System.Diagnostics.Debug.WriteLine(s);
            }





        }

    }
}