﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;


namespace SIK_NEW
{
    public partial class ProkerInput : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            
            if (Session["NIM"] == null)
            {
                Response.Redirect("Login.aspx");
            }

            SIKTableAdapters.PICTableAdapter tapic = new SIKTableAdapters.PICTableAdapter();

            SIK.PICDataTable dt = tapic.SelectPICOrganisasi(Session["NIM"].ToString());

            HKode.Value = dt.Rows[0]["KODE_ORGANISASI"].ToString();

            //LblPeriode.Text = dt.Rows[0]["PERIODE"].ToString(); 
            LblNamaOrgan.Text = dt.Rows[0]["NAMA_ORGANISASI"].ToString();
            LblPeriode.Text = Convert.ToDateTime(dt.Rows[0]["PERIODE"]).ToString("yyyy-MM-dd");
        }

        //randomizer
        public int RandomNumber(int min, int max)
        {
            Random random = new Random();
            return random.Next(min, max);
        }

        public string RandomString(int size, bool lowerCase)
        {
            StringBuilder builder = new StringBuilder();
            Random random = new Random();
            char ch;
            for (int i = 0; i < size; i++)
            {
                ch = Convert.ToChar(Convert.ToInt32(Math.Floor(26 * random.NextDouble() + 65)));
                builder.Append(ch);
            }
            if (lowerCase)
                return builder.ToString().ToLower();
            return builder.ToString();
        }

        public static string tmp = ""; //nama pdf yang udah di random

        protected void btSubmitProker_Click(object sender, EventArgs e)
        {

            System.Diagnostics.Debug.WriteLine(Session["NIM"]);

            SIKTableAdapters.PICTableAdapter tapic = new SIKTableAdapters.PICTableAdapter();

            SIK.PICDataTable dt = tapic.SelectPICOrganisasi(Session["NIM"].ToString());

            //ini datanya
            //  System.Diagnostics.Debug.WriteLine("PER: " + dt.Rows[0]["PERIODE"].ToString());
            //System.Diagnostics.Debug.WriteLine("PER: " + Convert.ToDateTime(dt.Rows[0]["PERIODE"]).ToString("yyyy-MM-dd"));
            //System.Diagnostics.Debug.WriteLine("ORG: " + dt.Rows[0]["KODE_ORGANISASI"].ToString());

            //System.Diagnostics.Debug.WriteLine("ORG: " + dt.PERIODEColumn);
            //System.Diagnostics.Debug.WriteLine("PERIODE: " + dt.Columns["PERIODE"][0]);


            //ambil input waktu


            string dateString = @iPelaksanaanStart.Value;
            DateTime inputcheck = Convert.ToDateTime(dateString, System.Globalization.CultureInfo.GetCultureInfo("id-ID").DateTimeFormat);

            DateTime timenow = Convert.ToDateTime(DateTime.Now, System.Globalization.CultureInfo.GetCultureInfo("id-ID").DateTimeFormat);
            int result = DateTime.Compare(inputcheck, timenow);
            //System.Diagnostics.Debug.WriteLine(inputcheck + " and " + timenow);
           
            //if days are later

            if (result == 1) 
            {
                TimeSpan ts = inputcheck - DateTime.Now;
                double NumberOfDays = ts.TotalDays;
                if (NumberOfDays >= 30)
                {
                    System.Diagnostics.Debug.WriteLine("hari lebih dari 30, boleh masuk proker");

                    //bikin algo masuk ke db disini

                    if (PDFProker.HasFile)
                    {
                        //System.Diagnostics.Debug.WriteLine("test");
                        string extensionPDF = System.IO.Path.GetExtension(PDFProker.FileName);

                        StringBuilder builder = new StringBuilder();
                        builder.Append(RandomString(4, true));
                        builder.Append(RandomNumber(1000, 9999));
                        builder.Append(RandomString(2, false));
                        string imgrandom = builder.ToString(); //img random itu hash nama file 
                        tmp = imgrandom + extensionPDF;

                        if (extensionPDF.ToLower() != ".pdf")
                        {
                            LblMessage.Text = "Tidak bisa upload karena harus .pdf";
                            return;
                        }

                        else if (extensionPDF.ToLower() == ".pdf")
                        {
                            

                            PDFProker.SaveAs(MapPath("~/uploads/" + imgrandom + extensionPDF));
                            LblMessage.Text = "File succeed Uploaded";
                            int danas = Convert.ToInt32(iDana.Value);

                            SIKTableAdapters.PROGRAM_KERJATableAdapter ta = new SIKTableAdapters.PROGRAM_KERJATableAdapter();
                            //ta.InsertProgramKerja(LblKodeOrgan.Text, LblPeriode.Text, "", "", "", "", "", "asd",0, "", false);

                            ta.InsertProgramKerja(HKode.Value, LblPeriode.Text, iJudulKegiatan.Value, iPelaksanaanStart.Value, iPelaksanaanFinish.Value, tmp,
                                iKetuaPelaksana.Value, iDeskripsi.Value, danas, iKetPeserta.Value, false, false);

                            Response.Redirect(Request.RawUrl);

                        }

                            
                    }

                    else
                    {
                        LblMessage.Text = "file kosong";
                        return;
                    }


                }
                else if (NumberOfDays < 30)
                {
                    System.Diagnostics.Debug.WriteLine("hari kurang dr 30, tdk masuk");

                    //bikin alert disini
                }


            }
            //if days are earlier
            else
            {
                System.Diagnostics.Debug.WriteLine("tanggal kurang dari datetime.now");
            }



















            //            System.Diagnostics.Debug.WriteLine(result);
            //get kode organisasi
            // SIKTableAdapters.MAHASISWATableAdapter tapic = new SIKTableAdapters.MAHASISWATableAdapter();
            //tapic.SelectMahasiswa




            //// string dana = Convert.ToString(iDana.Value);
            //Int32 dana = Convert.ToInt32(iDana.Value);
            //SIKTableAdapters.PROGRAM_KERJATableAdapter taproker = new SIKTableAdapters.PROGRAM_KERJATableAdapter();
            //taproker.InsertProgramKerja(ikodeOrgan.SelectedItem.Value, iPeriode.SelectedItem.Value, iJudulKegiatan.Value, iPelaksanaanStart.Value, iPelaksanaanFinish.Text, PDFProker.FileName, iKetuaPelaksana.Value, iDeskripsi.Value,dana, iKetPeserta.Value);

            ////yg alert 
            //Response.Write("<script language='javascript'>window.alert('Data berhasil terinput');window.location='ProkerInput.aspx';</script>");
            //Response.Redirect(Request.RawUrl);


        }

        protected void BtUpdateProker_Click(object sender, EventArgs e)
        {
            DivbtInsertUpdate.Visible = false;
            divProkerviewtabel.Visible = true;
        }


        protected void BtInsertProker_Click(object sender, EventArgs e)
        {
            divbtSubmitProker.Visible = true;
            DivInsertProkerform.Visible = true;
            DivbtInsertUpdate.Visible = false;
        }

        protected void GridView1_SelectedIndexChanged(object sender, EventArgs e)
        {
            //update Proker
            string ProkerID = (GvListProker.SelectedValue).ToString();
            hKodeProker.Value = ProkerID;
            DivbtInsertUpdate.Visible = false;
            divProkerviewtabel.Visible = false;
            divbtSubmitProker.Visible = false;
            DivInsertProkerform.Visible =true;
            divbtupdateproker.Visible = true;

            //dapetin value
            SIKTableAdapters.PROGRAM_KERJATableAdapter taProker = new SIKTableAdapters.PROGRAM_KERJATableAdapter();

            SIK.PROGRAM_KERJADataTable dt = taProker.SelectByKodeProker(ProkerID);

            //isi value di tb
            iJudulKegiatan.Value = dt.Rows[0]["JUDUL_KEGIATAN"].ToString();
            iPelaksanaanStart.Value = dt.Rows[0]["PELAKSANAAN_START"].ToString();
            iPelaksanaanFinish.Value = dt.Rows[0]["PELAKSANAAN_FINISH"].ToString();
            iKetuaPelaksana.Value = dt.Rows[0]["KETUA_PELAKSANA"].ToString();
            iDeskripsi.Value = dt.Rows[0]["DESKRIPSI_PROG"].ToString();
            iDana.Value = dt.Rows[0]["DANA"].ToString();
            iKetPeserta.Value = dt.Rows[0]["KETERANGAN_PESERTA"].ToString();
            pdfdelete = dt.Rows[0]["PDF_URL"].ToString();


        }

        public static string pdfdelete = "";

        protected void BtUpdateProker_Click1(object sender, EventArgs e)
        {
            string dateString = @iPelaksanaanStart.Value;
            DateTime inputcheck = Convert.ToDateTime(dateString, System.Globalization.CultureInfo.GetCultureInfo("id-ID").DateTimeFormat);

            DateTime timenow = Convert.ToDateTime(DateTime.Now, System.Globalization.CultureInfo.GetCultureInfo("id-ID").DateTimeFormat);
            int result = DateTime.Compare(inputcheck, timenow);

            if (result == 1)
            {
                TimeSpan ts = inputcheck - DateTime.Now;
                double NumberOfDays = ts.TotalDays;
                if (NumberOfDays >= 30)
                {
                    System.Diagnostics.Debug.WriteLine("hari lebih dari 30, boleh masuk proker");

                    //bikin algo masuk ke db disini
                    //update
                    if (PDFProker.HasFile)
                    {


                        //System.Diagnostics.Debug.WriteLine("test");
                        string extensionPDF = System.IO.Path.GetExtension(PDFProker.FileName);

                        StringBuilder builder = new StringBuilder();
                        builder.Append(RandomString(4, true));
                        builder.Append(RandomNumber(1000, 9999));
                        builder.Append(RandomString(2, false));
                        string imgrandom = builder.ToString(); //img random itu hash nama file 
                        tmp = imgrandom + extensionPDF;

                        if (extensionPDF.ToLower() != ".pdf")
                        {
                            LblMessage.Text = "Tidak bisa upload karena harus .pdf";
                            return;
                        }

                        else if (extensionPDF.ToLower() == ".pdf")
                        {
                    
                        int danas = Convert.ToInt32(iDana.Value);

                        SIKTableAdapters.PROGRAM_KERJATableAdapter ta = new SIKTableAdapters.PROGRAM_KERJATableAdapter();

                        System.Diagnostics.Debug.WriteLine(pdfdelete);


                        string imageFilePath = Server.MapPath("~/uploads/" + pdfdelete);
                        System.IO.File.Delete(imageFilePath);


                        PDFProker.SaveAs(MapPath("~/uploads/" + imgrandom + extensionPDF));
                        LblMessage.Text = "File succeed Uploaded";

                        ta.UpdateProker1(iJudulKegiatan.Value, iPelaksanaanStart.Value, iPelaksanaanFinish.Value, tmp, //tmp nama img yang udh dirandom
                        iKetuaPelaksana.Value, iDeskripsi.Value, danas, iKetPeserta.Value, hKodeProker.Value);

                        Response.Redirect(Request.RawUrl);

                        }


                    }       

                    else
                    {
                    int danas = Convert.ToInt32(iDana.Value);

                    SIKTableAdapters.PROGRAM_KERJATableAdapter ta = new SIKTableAdapters.PROGRAM_KERJATableAdapter();
                    ta.UpdateProkerTanpaPDF1(iJudulKegiatan.Value, iPelaksanaanStart.Value, iPelaksanaanFinish.Value,
                    iKetuaPelaksana.Value, iDeskripsi.Value, danas, iKetPeserta.Value, hKodeProker.Value);

                    Response.Redirect(Request.RawUrl);
                    }

                }
                else if (NumberOfDays < 30)
                {
                    System.Diagnostics.Debug.WriteLine("hari kurang dr 30, tdk masuk");

                    //bikin alert disini
                }


            }
            //if days are earlier
            else
            {
                System.Diagnostics.Debug.WriteLine("tanggal kurang dari datetime.now");
            }
        }

        
    }
}