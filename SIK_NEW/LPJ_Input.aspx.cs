﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;

namespace SIK_NEW
{
    public partial class LPJ_Input : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            if (Session["NIM"] == null)
            {
                Response.Redirect("Login.aspx");
            }
        }
        //randomizer
        public int RandomNumber(int min, int max)
        {
            Random random = new Random();
            return random.Next(min, max);
        }

        public string RandomString(int size, bool lowerCase)
        {
            StringBuilder builder = new StringBuilder();
            Random random = new Random();
            char ch;
            for (int i = 0; i < size; i++)
            {
                ch = Convert.ToChar(Convert.ToInt32(Math.Floor(26 * random.NextDouble() + 65)));
                builder.Append(ch);
            }
            if (lowerCase)
                return builder.ToString().ToLower();
            return builder.ToString();
        }

        public static string tmp_lpj = ""; //nama pdf yang udah di random
        protected void BtLPJsubmit_Click(object sender, EventArgs e)
        {

            if (PDFUploadLPJ.HasFile)
            {
                //System.Diagnostics.Debug.WriteLine("test");
                string extensionPDF = System.IO.Path.GetExtension(PDFUploadLPJ.FileName);

                StringBuilder builder = new StringBuilder();
                builder.Append(RandomString(4, true));
                builder.Append(RandomNumber(1000, 9999));
                builder.Append(RandomString(2, false));
                string imgrandom = builder.ToString(); //img random itu hash nama file 
                tmp_lpj = imgrandom + extensionPDF;

                if (extensionPDF.ToLower() != ".pdf")
                {
                    LblMessage.Text = "Tidak bisa upload karena harus .pdf";
                    return;
                }

                else if (extensionPDF.ToLower() == ".pdf")
                {
                    PDFUploadLPJ.SaveAs(MapPath("~/uploads/" + imgrandom + extensionPDF));
                    LblMessage.Text = "File succeed Uploaded";
                    DateTime dtime = Convert.ToDateTime(DateTime.Now, System.Globalization.CultureInfo.GetCultureInfo("id-ID").DateTimeFormat);
                    int danasisa = Convert.ToInt32(iDanasisa.Value);

                    SIKTableAdapters.LPJTableAdapter talpj = new SIKTableAdapters.LPJTableAdapter();
                    talpj.InsertLPJ(ddSIK.SelectedValue, dtime.ToString(), danasisa, tmp_lpj,false);

                    ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('Data Berhasil Masuk ke DB');", true);
                    Response.Redirect(Request.RawUrl);

                }
            }

            else
            {
                LblMessage.Text = "file kosong";
                return;
            }
           // int danasisa = Convert.ToInt32(iDanasisa.Value);
            //SIKTableAdapters.LPJTableAdapter talpj = new SIKTableAdapters.LPJTableAdapter();
          //  talpj.InsertLPJ(ddSIK.SelectedValue, tglSrt.Value, danasisa, PDFUploadLPJ.FileName);

        }

        public static string hkodeLPJ = "";
        public static string pdfdelete = "";

        protected void GridView1_SelectedIndexChanged(object sender, EventArgs e)
        {
            hkodeLPJ = GvListLPJ.SelectedValue.ToString();
            LblNoLPJ.Text = hkodeLPJ;
            divlpjview.Visible = false;
            divinputLPJ.Visible = true;
            DivBtLPJSubmit.Visible = false;
            divBtLPJUpdate.Visible = true;
            LblNoLPJ.Visible = true;
            ddSIK.Visible = false;
            Label1.Visible = false;

            SIKTableAdapters.LPJTableAdapter taProker = new SIKTableAdapters.LPJTableAdapter();

            SIK.LPJDataTable dt = taProker.SelectLPJUpdate(hkodeLPJ);

            iDanasisa.Value = dt.Rows[0]["DANA_SISA"].ToString();
            pdfdelete= dt.Rows[0]["PDF_BUKTI_TRANSAKSI"].ToString();

            


        }

        public static string tmp = "";

        protected void BtUpdate_Click(object sender, EventArgs e)
        {
            if (PDFUploadLPJ.HasFile)
            {


                //System.Diagnostics.Debug.WriteLine("test");
                string extensionPDF = System.IO.Path.GetExtension(PDFUploadLPJ.FileName);

                StringBuilder builder = new StringBuilder();
                builder.Append(RandomString(4, true));
                builder.Append(RandomNumber(1000, 9999));
                builder.Append(RandomString(2, false));
                string imgrandom = builder.ToString(); //img random itu hash nama file 
                tmp = imgrandom + extensionPDF;

                if (extensionPDF.ToLower() != ".pdf")
                {
                    LblMessage.Text = "Tidak bisa upload karena harus .pdf";
                    return;
                }

                else if (extensionPDF.ToLower() == ".pdf")
                {

                    int danas = Convert.ToInt32(iDanasisa.Value);

                    SIKTableAdapters.LPJTableAdapter ta = new SIKTableAdapters.LPJTableAdapter();

                    System.Diagnostics.Debug.WriteLine(pdfdelete);


                    string imageFilePath = Server.MapPath("~/uploads/" + pdfdelete);
                    System.IO.File.Delete(imageFilePath);


                    PDFUploadLPJ.SaveAs(MapPath("~/uploads/" + imgrandom + extensionPDF));
                    LblMessage.Text = "File succeed Uploaded";

                    ta.UpdateLPJ(danas, tmp, hkodeLPJ);

                    Response.Redirect(Request.RawUrl);

                }


            }

            else
            {
                int danas = Convert.ToInt32(iDanasisa.Value);

                SIKTableAdapters.LPJTableAdapter ta = new SIKTableAdapters.LPJTableAdapter();
                ta.UpdateLPJTanpaPDF(danas, hkodeLPJ);

                Response.Redirect(Request.RawUrl);
            }
        }

        protected void BtInsertLPJJust_Click(object sender, EventArgs e)
        {
            divlpjview.Visible = false;
            divinputLPJ.Visible = true;
            DivBtLPJSubmit.Visible = true;
            BtInsertLPJJust.Visible = false;
        }
    }
}
