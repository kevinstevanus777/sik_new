﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterHomepage.Master" AutoEventWireup="true" CodeBehind="ProkerInput.aspx.cs" Inherits="SIK_NEW.ProkerInput" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="TopPage" runat="server">

    <form runat="server">
    <h1>Input Program Kerja</h1>

    <div class="col-md-5">
        <!-- Kode Organisasi <br />
<asp:DropDownList ID="ikodeOrgan" runat="server" DataSourceID="organisasi" DataValueField="KODE_ORGANISASI"  DataTextField="NAMA_ORGANISASI" Width="251px" AutoPostBack="True">
    </asp:DropDownList>

    <asp:SqlDataSource ID="organisasi" runat="server" ConnectionString="<%$ ConnectionStrings:SIKConnectionString %>" SelectCommand="SELECT [NAMA_ORGANISASI], [KODE_ORGANISASI] FROM [ORGANISASI]"></asp:SqlDataSource>
    
         <asp:Label ID="Label1" runat="server" Text="Label"></asp:Label>
    
    <br />  
    Periode <br />
<asp:DropDownList ID="iPeriode" runat="server" DataSourceID="Periode" DataTextField="PERIODE" DataValueField="PERIODE" Width="251px">
    </asp:DropDownList>

    <asp:SqlDataSource ID="Periode" runat="server" ConnectionString="<%$ ConnectionStrings:SIKConnectionString %>" SelectCommand="SELECT [KODE_ORGANISASI], [PERIODE] FROM [ORGANISASI_PERIODE] WHERE ([KODE_ORGANISASI] = @KODE_ORGANISASI)">
        <SelectParameters>
            <asp:ControlParameter ControlID="ikodeOrgan" Name="KODE_ORGANISASI" PropertyName="SelectedValue" Type="String" />
        </SelectParameters>
    </asp:SqlDataSource>
    -->
        <h4>Organisasi
            <asp:Label ID="LblNamaOrgan" runat="server" Text="Label"></asp:Label>
            <asp:HiddenField ID="HKode" runat="server" />
            <br />
            periode
        
         <asp:Label ID="LblPeriode" runat="server" Text="Label"></asp:Label>
            <br />
        </h4>
        <div id="DivbtInsertUpdate" visible="true" runat="server">
            <asp:Button ID="BtInsert" runat="server" OnClick="BtInsertProker_Click" Text="Insert Program Kerja" />
            <asp:Button ID="btupdate" runat="server" Text="Update Program Kerja" OnClick="BtUpdateProker_Click" />
            <br />
        </div>

        <div id="DivInsertProkerform" visible="false" runat="server">
        <div class="form-group">
            <label for="exampleInputEmail1">Judul Kegiatan</label>
            <input type="text" class="form-control" id="iJudulKegiatan" runat="server" aria-describedby="emailHelp" placeholder="Judul Kegiatan">
        </div>


        <div class="form-group">
            <label for="exampleInputPst">Pelaksanaan Start</label>
            <br />

            <input type="text" id="iPelaksanaanStart" runat="server" clientidmode="Static" readonly="readonly">
        </div>

        <div class="form-group">
            <label for="exampleInputPFh">Pelaksanaan Finish</label>
            <input type="text" id="iPelaksanaanFinish" runat="server" clientidmode="Static" readonly="readonly"/>

            <%--<br />
    Ketua Pelaksana <br />
    <asp:DropDownList ID="iKetuaPelaksana" runat="server" DataSourceID="PIC" DataTextField="NIM" DataValueField="NIM" Width="267px">
    </asp:DropDownList>

    <br />
    <asp:SqlDataSource ID="PIC" runat="server" ConnectionString="<%$ ConnectionStrings:SIKConnectionString %>" SelectCommand="SELECT [NIM] FROM [PIC] WHERE (([KODE_ORGANISASI] = @KODE_ORGANISASI) AND ([PERIODE] = @PERIODE))">
        <SelectParameters>
            <asp:ControlParameter ControlID="ikodeOrgan" Name="KODE_ORGANISASI" PropertyName="SelectedValue" Type="String" />
            <asp:ControlParameter ControlID="iPeriode" DbType="Date" Name="PERIODE" PropertyName="SelectedValue" />
        </SelectParameters>
    </asp:SqlDataSource>
        --%><%--<asp:Button ID="btNelson" runat="server" Text="nelson" OnClick="btNelson_Click" />--%>
        </div>


        Lampiran PDF Program Kerja
        <br />
        <asp:FileUpload ID="PDFProker" runat="server" />
        <br />
        <asp:Label ID="LblMessage" runat="server" Text="Label"></asp:Label>


        <div class="form-group">
            <label for="exampleInputEmail1">Ketua Pelaksana </label>
            <input type="text" class="form-control" id="iKetuaPelaksana" runat="server" aria-describedby="emailHelp" placeholder="Input Ketua Pelaksana">
        </div>

        <div class="form-group">
            <label for="exampleInputEmail1">Deskripsi </label>
            <input type="text" class="form-control" id="iDeskripsi" runat="server" aria-describedby="emailHelp" placeholder="Input Deskripsi Program Kerja">
        </div>

        <div class="form-group">
            <label for="exampleInputEmail1">Dana</label>
            <input type="number" min="0" class="form-control" id="iDana" runat="server" aria-describedby="emailHelp" placeholder="Input Dana Bipekskur">
        </div>
        <br />
        <div class="form-group">
            <label for="exampleInputEmail1">Keterangan Peserta</label>
            <input type="text" class="form-control" id="iKetPeserta" runat="server" aria-describedby="emailHelp" placeholder="Input Keterangan Peserta">
        </div>

            </div>
        <%--<br />
    Ketua Pelaksana <br />
    <asp:DropDownList ID="iKetuaPelaksana" runat="server" DataSourceID="PIC" DataTextField="NIM" DataValueField="NIM" Width="267px">
    </asp:DropDownList>

    <br />
    <asp:SqlDataSource ID="PIC" runat="server" ConnectionString="<%$ ConnectionStrings:SIKConnectionString %>" SelectCommand="SELECT [NIM] FROM [PIC] WHERE (([KODE_ORGANISASI] = @KODE_ORGANISASI) AND ([PERIODE] = @PERIODE))">
        <SelectParameters>
            <asp:ControlParameter ControlID="ikodeOrgan" Name="KODE_ORGANISASI" PropertyName="SelectedValue" Type="String" />
            <asp:ControlParameter ControlID="iPeriode" DbType="Date" Name="PERIODE" PropertyName="SelectedValue" />
        </SelectParameters>
    </asp:SqlDataSource>
        --%>
        <%--<asp:Button ID="btNelson" runat="server" Text="nelson" OnClick="btNelson_Click" />--%>

        <div id="divbtSubmitProker" visible="false" runat="server">
         <asp:Button ID="btSubmitProker" runat="server" CssClass="btn btn-primary" OnClick="btSubmitProker_Click" Text="Submit" />
           
        </div>

        <div id="divbtupdateproker" visible="false" runat="server">
        <asp:Button ID="BtUpdateProker" runat="server" Text="Update" OnClick="BtUpdateProker_Click1" />
            </div>


        <div id="divProkerviewtabel" visible="false" runat="server">
        <asp:GridView ID="GvListProker" runat="server" AutoGenerateColumns="False" DataSourceID="ProkerViewDtSource" OnSelectedIndexChanged="GridView1_SelectedIndexChanged" DataKeyNames="KODE_PROGRAM">
            <Columns>
                <asp:CommandField ShowSelectButton="True" SelectText="Edit" />
                <asp:BoundField DataField="KODE_PROGRAM" HeaderText="KODE_PROGRAM" ReadOnly="True" SortExpression="KODE_PROGRAM" />
                <asp:BoundField DataField="PERIODE" HeaderText="PERIODE" SortExpression="PERIODE" />
                <asp:BoundField DataField="JUDUL_KEGIATAN" HeaderText="JUDUL_KEGIATAN" SortExpression="JUDUL_KEGIATAN" />
                <asp:BoundField DataField="KETUA_PELAKSANA" HeaderText="KETUA_PELAKSANA" SortExpression="KETUA_PELAKSANA" />
                <asp:BoundField DataField="ALASAN" HeaderText="ALASAN" SortExpression="ALASAN" />
                <asp:CheckBoxField DataField="STATUS_PROKER" HeaderText="STATUS_PROKER" SortExpression="STATUS_PROKER" />
                <asp:BoundField DataField="DANA" HeaderText="DANA" SortExpression="DANA" />

            </Columns>
        </asp:GridView>


        <asp:SqlDataSource ID="ProkerViewDtSource" runat="server" ConnectionString="<%$ ConnectionStrings:SIKConnectionString %>" SelectCommand="SELECT [KODE_PROGRAM], [PERIODE], [JUDUL_KEGIATAN], [KETUA_PELAKSANA], [ALASAN], [STATUS_PROKER], [DANA] FROM [PROGRAM_KERJA] WHERE (([KODE_ORGANISASI] = @KODE_ORGANISASI) AND ([PERIODE] = @PERIODE))">
            <SelectParameters>
                <asp:ControlParameter ControlID="HKode" Name="KODE_ORGANISASI" PropertyName="Value" Type="String" />
                <asp:ControlParameter ControlID="LblPeriode" DbType="Date" Name="PERIODE" PropertyName="Text" />
            </SelectParameters>
        </asp:SqlDataSource>

            <asp:HiddenField ID="hKodeProker" runat="server" />

            </div>

        <br />



        </div>
    </form>



    <!-- js untuk datepicker -->
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link rel="stylesheet" href="/resources/demos/style.css">
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>




    <script>

        $(function () {
            $("#iPelaksanaanStart").datepicker({ dateFormat:'yy-mm-dd', minDate:31});
            // $("#iStartdate").datepicker();
            //$("iFinishdate").datepicker();
        });
        $(function () {

            $('#iPelaksanaanFinish').datepicker({ dateFormat:'yy-mm-dd', minDate:31});
        });



    </script>




</asp:Content>
