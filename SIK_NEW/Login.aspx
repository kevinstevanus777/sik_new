﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterHomepage.Master" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="SIK_NEW.WebForm4" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="TopPage" runat="server">

        <div class="col-6">
        <form runat="server">

            <!-- tambahan dari kevin -->


        
            <div class="form-group">
                <label for="exampleInputPassword1">NAMA</label>
                <input type="text" class="form-control" id="iNama" runat="server" placeholder="Your Name">
            </div>

            
            <div id="cError" class="alert alert-danger alert-dismissible mb-2" role="alert" runat="server" visible="false">
                <asp:Label ID="oError" runat="server" Text=""></asp:Label>

                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <asp:Button ID="btLogin" CssClass="btn btn-primary" runat="server" OnClick="btLogin_Click" Text="Login" />
            <asp:Button ID="btSignUp" CssClass="btn btn-primary" runat="server" OnClick="btSignUp_Click" Text="Sign Up" />

            <br />
            <br />
            <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" DataSourceID="DSLogin" >
            <Columns>
                    <asp:BoundField DataField="NIM" HeaderText="NIM" ReadOnly="True" SortExpression="NIM" />
                    <asp:BoundField DataField="NAMA" HeaderText="NAMA" SortExpression="NAMA" />

            </Columns>
            </asp:GridView>
                 <asp:SqlDataSource ID="DSLogin" runat="server" ConnectionString="<%$ ConnectionStrings:SIKConnectionString %>" SelectCommand="SELECT NIM,NAMA FROM MAHASISWA"></asp:SqlDataSource>
       
                
            <br />

        </form>

    </div>
</asp:Content>
