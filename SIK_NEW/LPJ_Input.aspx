﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterHomepage.Master" AutoEventWireup="true" CodeBehind="LPJ_Input.aspx.cs" Inherits="SIK_NEW.LPJ_Input" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="TopPage" runat="server">
    

     <form runat="server">
            <h1>Laporan Pertanggung Jawab</h1>
            <asp:Button ID="BtInsertLPJJust" runat="server" OnClick="BtInsertLPJJust_Click" Text="Insert LPJ" />
         <br />
         <div id="divlpjview" runat="server" visible="true">
            <asp:GridView ID="GvListLPJ" runat="server" AutoGenerateColumns="False" DataSourceID="SqlLPJView" OnSelectedIndexChanged="GridView1_SelectedIndexChanged" DataKeyNames="NO_SURAT_LPJ">
                <Columns>
                    <asp:CommandField ShowSelectButton="True" />
                    <asp:BoundField DataField="NO_SURAT_LPJ" HeaderText="NO_SURAT_LPJ" ReadOnly="True" SortExpression="NO_SURAT_LPJ" />
                    <asp:BoundField DataField="NOMOR_SIK" HeaderText="NOMOR_SIK" SortExpression="NOMOR_SIK" />
                    <asp:BoundField DataField="NIK" HeaderText="NIK" SortExpression="NIK" />
                    <asp:BoundField DataField="TANGGAL_SURAT" HeaderText="TANGGAL_SURAT" SortExpression="TANGGAL_SURAT" />
                    <asp:BoundField DataField="ALASAN" HeaderText="ALASAN" SortExpression="ALASAN" />
                    <asp:BoundField DataField="DANA_SISA" HeaderText="DANA_SISA" SortExpression="DANA_SISA" />
                    <asp:BoundField DataField="PDF_BUKTI_TRANSAKSI" HeaderText="PDF_BUKTI_TRANSAKSI" SortExpression="PDF_BUKTI_TRANSAKSI" />
                </Columns>
            </asp:GridView>
            <asp:SqlDataSource ID="SqlLPJView" runat="server" ConnectionString="<%$ ConnectionStrings:SIKConnectionString %>" SelectCommand="SELECT [NO_SURAT_LPJ], [NOMOR_SIK], [NIK], [TANGGAL_SURAT], [ALASAN], [DANA_SISA], [PDF_BUKTI_TRANSAKSI] FROM [LPJ]"></asp:SqlDataSource>
             <br />
         <br />

             </div>
            <asp:Label ID="LblNoLPJ" runat="server" Text="-" Visible="false"></asp:Label>
         <br />
         <br />

         <div id="divinputLPJ" runat="server" visible="false">
            <p> 
                <asp:Label ID="Label1" runat="server" Text="Judul SIK"></asp:Label>
             </p>
              <asp:DropDownList ID="ddSIK" DataSourceID="SqlSIK" DataValueField="NOMOR_SIK" DataTextField="JUDUL"   runat="server" CssClass="form-control" AutoPostBack="True">
        </asp:DropDownList>

        <asp:SqlDataSource ID="SqlSIK" runat="server" ConnectionString='<%$ ConnectionStrings:SIKConnectionString %>' SelectCommand="SELECT JUDUL, S.NOMOR_SIK
from LPJ L RIGHT JOIN SIK S ON L.NOMOR_SIK = S.NOMOR_SIK RIGHT JOIN PROPOSAL P ON S.NO_SURAT_PROPOSAL=P.NO_SURAT_PROPOSAL
RIGHT JOIN PROGRAM_KERJA PK ON PK.KODE_PROGRAM = P.KODE_PROGRAM RIGHT JOIN ORGANISASI O ON PK.KODE_ORGANISASI = O.KODE_ORGANISASI
RIGHT JOIN PIC PIC ON PIC.KODE_ORGANISASI = O.KODE_ORGANISASI RIGHT JOIN MAHASISWA M ON PIC.NIM = M.NIM WHERE PIC.NIM = @MHS AND L.NO_SURAT_LPJ IS NULL AND S.PDF_URL IS NOT NULL">
            <SelectParameters>
                <asp:SessionParameter Name="MHS" SessionField="NIM" />
            </SelectParameters>
            </asp:SqlDataSource>
            

        <div class="form-group">
            &nbsp;
        </div>

    

       <div class="form-group">
            <label for="exampleInputdana">Dana Sisa</label>
            <input type="number" min="0" max="2147483647" class="form-control" id="iDanasisa" runat="server" aria-describedby="emailHelp" placeholder="Input Sisa Dana">
        </div>

           <div class="form-group">
                    <label for="file">Upload File PDF </label>
                     <div class="col-sm-6">
                          <asp:FileUpload ID="PDFUploadLPJ" runat="server" />
                             <br />
                         <asp:Label ID="LblMessage" runat="server" Text="Label"></asp:Label>

                    </div>
                </div>
             </div>
         <div id="DivBtLPJSubmit" runat="server" visible="false">
    <asp:Button ID="BtLPJsubmit" CssClass="btn btn-primary" OnClick="BtLPJsubmit_Click" runat="server" Text="Submit" />
    </div>

         <div id="divBtLPJUpdate" runat="server" visible="false">
    
             <asp:Button ID="BtUpdate" runat="server" Text="Update" OnClick="BtUpdate_Click" />
    
    </div>
             
</form>





    

    <!-- js untuk datepicker -->
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link rel="stylesheet" href="/resources/demos/style.css">
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>




    <script>

        $(function () {

            $('#tglSrt').datepicker({ dateFormat: 'yy-mm-dd', minDate:0 });
        });
        </script>

</asp:Content>
