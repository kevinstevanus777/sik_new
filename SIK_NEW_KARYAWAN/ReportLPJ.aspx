﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterSite.Master" AutoEventWireup="true" CodeBehind="ReportLPJ.aspx.cs" Inherits="SIK_NEW_KARYAWAN.ReportLPJ" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="TopPage" runat="server">
    <h1>Report Laporan Pertanggung Jawaban</h1>

    <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" DataSourceID="SqlDataSource1">
        <Columns>
            <asp:BoundField DataField="NO_SURAT_LPJ" HeaderText="NO_SURAT_LPJ" ReadOnly="True" SortExpression="NO_SURAT_LPJ" />
            <asp:BoundField DataField="TANGGAL_SURAT" HeaderText="TANGGAL_SURAT" SortExpression="TANGGAL_SURAT" />
            <asp:BoundField DataField="PELAKSANAAN_START" HeaderText="PELAKSANAAN_START" SortExpression="PELAKSANAAN_START" />
            <asp:BoundField DataField="PELAKSANAAN_FINISH" HeaderText="PELAKSANAAN_FINISH" SortExpression="PELAKSANAAN_FINISH" />
            <asp:BoundField DataField="TEMPAT" HeaderText="TEMPAT" SortExpression="TEMPAT" />
            <asp:BoundField DataField="DANA_TERPAKAI" HeaderText="DANA_TERPAKAI" ReadOnly="True" SortExpression="DANA_TERPAKAI" />
            <asp:BoundField DataField="STATUS" HeaderText="STATUS" ReadOnly="True" SortExpression="STATUS" />
        </Columns>
    </asp:GridView>
    <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:SIKConnectionString %>" SelectCommand="SELECT NO_SURAT_LPJ, L.TANGGAL_SURAT, PELAKSANAAN_START, PELAKSANAAN_FINISH, TEMPAT, (DANA + DANA_BANTUAN) - DANA_SISA AS DANA_TERPAKAI, CASE
WHEN DANA_SISA &lt; 0 THEN 'KURANG'
WHEN DANA_SISA &gt; 0 THEN 'LEBIH'
ELSE 'PAS'	
END AS STATUS
FROM LPJ L, PROGRAM_KERJA PK, PROPOSAL P, SIK S
WHERE L.NOMOR_SIK = S.NOMOR_SIK AND P.NO_SURAT_PROPOSAL = S.NO_SURAT_PROPOSAL AND P.KODE_PROGRAM = PK.KODE_PROGRAM
"></asp:SqlDataSource>


</asp:Content>
