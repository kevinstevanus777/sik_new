﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterSite.Master" AutoEventWireup="true" CodeBehind="SIK_Input.aspx.cs" Inherits="SIK_NEW_KARYAWAN.SIK_Input" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="TopPage" runat="server">
     <h1>Surat Izin Kegiatan</h1>
    <form>
        <asp:Label ID="Label2" runat="server" Text="-"></asp:Label>
        <br />

        <div id="divSIKview" runat="server" visible="true">
        List SIK<br />
        <asp:GridView ID="GridListSIK" runat="server" AutoGenerateColumns="False" DataSourceID="ListSIKGrid" OnSelectedIndexChanged="GridListSIK_SelectedIndexChanged">
            <Columns>
                <asp:CommandField ShowSelectButton="True" />
                <asp:BoundField DataField="NOMOR_SIK" HeaderText="NOMOR_SIK" ReadOnly="True" SortExpression="NOMOR_SIK" />
                <asp:BoundField DataField="NO_SURAT_PROPOSAL" HeaderText="NO_SURAT_PROPOSAL" SortExpression="NO_SURAT_PROPOSAL" />
                <asp:BoundField DataField="PERIHAL" HeaderText="PERIHAL" SortExpression="PERIHAL" />
                <asp:BoundField DataField="TANGGAL_SURAT" HeaderText="TANGGAL_SURAT" SortExpression="TANGGAL_SURAT" />
                <asp:BoundField DataField="PDF_URL" HeaderText="PDF_URL" SortExpression="PDF_URL" />
            </Columns>
        </asp:GridView>
        <asp:SqlDataSource ID="ListSIKGrid" runat="server" ConnectionString="<%$ ConnectionStrings:SIKConnectionString %>" SelectCommand="SELECT [NOMOR_SIK], [NO_SURAT_PROPOSAL], [PERIHAL], [TANGGAL_SURAT], [PDF_URL] FROM [SIK]"></asp:SqlDataSource>
        <asp:HiddenField ID="hKodeSIK" runat="server" />


            
        <br />
            <asp:Button ID="BtInsertJust" runat="server" OnClick="BtInsertJust_Click" Text="Insert SIK" />
        <br />

        <br />
        <asp:SqlDataSource ID="SdSJudulProposal" runat="server" ConnectionString="<%$ ConnectionStrings:SIKConnectionString %>" SelectCommand="SELECT P.JUDUL, P.NO_SURAT_PROPOSAL
FROM PROPOSAL P LEFT JOIN SIK S ON P.NO_SURAT_PROPOSAL = S.NO_SURAT_PROPOSAL
WHERE S.NOMOR_SIK IS NULL AND P.STATUS_PROPOSAL= 1 AND P.FISIK_IS_RECEIVED = 1"></asp:SqlDataSource>
        
        <br />

            </div>

        <asp:Button ID="UploadPDFjust" runat="server" OnClick="UploadPDFjust_Click" Text="Upload PDF" visible="false"/>
        <asp:Button ID="BtUpdateJust" runat="server" Text="Update SIK" visible="false" OnClick="BtUpdateJust_Click"/>
        <br />
        <br />

        <div id="divgenerate" runat="server" visible="false">
            <asp:Button ID="generatepdf" runat="server" OnClick="generatepdf_Click" Text="Generate PDF" />

            <br />
            <br />

        </div>

        <div id="divuploadPDF" runat="server" visible="false">
        
            <br />
            <br />

             <div class="form-group">
            <label for="file">Upload File PDF </label>
            <div class="col-sm-6">
                <asp:FileUpload ID="PDFUploadSIK" runat="server" />
                <br />
                <asp:Label ID="Label1" runat="server" Text="Label"></asp:Label>

                <br />
                <br />
                <asp:Button ID="BtSubmitPDF" runat="server" Text="Submit SIK" OnClick="BtSubmitPDF_Click" />

                <br />

            </div>
        </div>

        </div>



            

           


        <div id="divInputSIK" runat="server" visible="false">
         <div class="form-group">
             <h2>Input SIK</h2>
            <div runat="server" id="dvProposal" visible="true">

                <div id="divDDproposal" runat="server" visible="true">
                <label for="exampleInputProposal"> Proposal</label>
                <asp:DropDownList ID="DDProposal" runat="server" CssClass="form-control" DataSourceID="SdSJudulProposal" DataTextField="JUDUL" DataValueField="NO_SURAT_PROPOSAL"></asp:DropDownList>
                    </div>
            </div>
        </div>
             <br />
         <div class="form-group">
            <label for="exampleInputPerihal">Perihal</label>
            <input type="text" class="form-control" id="iPerihal" runat="server" aria-describedby="emailHelp" placeholder="Input Perihal">
        </div>
            <br />
            <div class="form-group">
            <label for="exampleInputPerihal">Direktorat Kemahasiswaan dan Alumni </label>
            <input type="text" class="form-control" id="TTD" runat="server" aria-describedby="emailHelp" placeholder="Direktorat Kemahasiswaan dan Alumni">
        </div>
         <br />
            <div class="form-group">
            <label for="exampleInputPerihal">Lampiran</label>
                <br />
            <asp:TextBox id="iLampiran" TextMode="multiline" Columns="50" Rows="5" runat="server" MaxLength="50" />
          </div>


        

            </div>
        <div id="BtSubmitSIKJUST" runat="server" visible ="false">
        <asp:Button ID="ButtonSubmitSIK" CssClass="btn btn-primary" runat="server" Text="Submit" Onclick="ButtonSubmitSIK_Click" />
            
        </div>

        <div id="divBtUpdateSIK" runat="server" visible="false">
            <asp:Button ID="BtUpdateSIK" runat="server" Text="Update" OnClick="BtUpdateSIK_Click" />
        </div>
        
    </form>


</asp:Content>
