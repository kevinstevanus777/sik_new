﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterSite.Master" AutoEventWireup="true" CodeBehind="Organisasi.aspx.cs" Inherits="SIK_NEW_KARYAWAN.WebForm5" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="TopPage" runat="server">


    <div class="col-md-5">
        <%--gridview organisasi(saja)--%>
        <div visible="false" runat="server" id="divOrganisasiInput">

            <%--tombol edit/select--%>
            <h1>ASAL ORGANISASI</h1>
            <asp:DropDownList ID="DDJenisOrganisasi" runat="server" CssClass="form-control" OnTextChanged="DDJenisOrganisasi_TextChanged" AutoPostBack="true">

                <asp:ListItem Text="-" Value=""></asp:ListItem>
                <asp:ListItem Text="Fakultas" Value="Fakultas"></asp:ListItem>
                <asp:ListItem Text="Prodi" Value="Prodi"></asp:ListItem>
                <asp:ListItem Text="Mandiri" Value="Mandiri"></asp:ListItem>
            </asp:DropDownList>

            <div class="form-group">


                <%--form input organisasi periode--%>
                <div runat="server" id="divFakultas" visible="false">
                    <label for="exampleInputEmail1">Fakultas</label>
                    <asp:DropDownList ID="oFakultas" DataSourceID="SqlFakultas" DataValueField="KODE_FAKULTAS" DataTextField="NAMA_FAKULTAS" runat="server" CssClass="form-control" AutoPostBack="true"></asp:DropDownList>
                    <asp:SqlDataSource ID="SqlFakultas" runat="server" ConnectionString='<%$ ConnectionStrings:SIKConnectionString %>' SelectCommand="SELECT * FROM FAKULTAS"></asp:SqlDataSource>
                </div>

                <%--button add new organisasi--%>
                <div runat="server" id="divProdi" visible="false">
                    <label for="exampleInputEmail1">Prodi</label>
                    <asp:DropDownList ID="oProdi" DataSourceID="SqlProdi" DataValueField="KODE_PRODI" DataTextField="NAMA_PRODI" runat="server" CssClass="form-control" AutoPostBack="true"></asp:DropDownList>
                    <asp:SqlDataSource ID="SqlProdi" runat="server" ConnectionString='<%$ ConnectionStrings:SIKConnectionString %>' SelectCommand="SELECT * FROM PRODI P, FAKULTAS F WHERE P.KODE_FAKULTAS = F.KODE_FAKULTAS AND P.KODE_FAKULTAS = @KODE_FAKULTAS ">

                        <SelectParameters>
                            <asp:ControlParameter ControlID="oFakultas" PropertyName="SelectedValue" Name="KODE_FAKULTAS" />
                        </SelectParameters>
                    </asp:SqlDataSource>
                </div>

            </div>

            <div class="form-group">
                <label for="exampleInputEmail1">Nama Organisasi</label>
                <input type="text" class="form-control" id="iNamaOrganisasi" runat="server" aria-describedby="emailHelp" placeholder="Nama Organisasi" maxlength="50">
            </div>

            <%--gridview organisasi(saja)--%>
            <asp:Button ID="btAddOrganisasi" runat="server" Text="Add New" CssClass="btn btn-primary" OnClick="btAddOrganisasi_Click" />

        </div>


        <%--tombol edit/select--%>
        <div id="divViewListPeriodeOrganisasi" runat="server" visible="false">
            <h1 runat="server" id="oKodeOrganisasi"></h1>
            <asp:HiddenField ID="hKodeOrganisasi" runat="server" Visible="false" />
            <p>view periode organisasi</p>
            <asp:Button ID="btAddPeriode" CssClass="btn btn-primary" runat="server" Text="Tambah Periode Baru" OnClick="btAddPeriode_Click" />
            <asp:SqlDataSource ID="SqlListPeriode" ConnectionString='<%$ ConnectionStrings:SIKConnectionString %>' SelectCommand="SELECT * FROM ORGANISASI_PERIODE WHERE KODE_ORGANISASI = @KODE_ORGANISASI " runat="server">
                <SelectParameters>
                    <asp:ControlParameter ControlID="hKodeOrganisasi" Name="KODE_ORGANISASI" Type="String" PropertyName="Value" />
                </SelectParameters>
            </asp:SqlDataSource>

            <asp:GridView ID="gvListPeriodeList" 
                CssClass="table-active" 
                DataKeyNames="KODE_ORGANISASI" 
                DataSourceID="SqlListPeriode" 
                runat="server" 
                OnSelectedIndexChanged="gvListPeriodeList_SelectedIndexChanged">
                <Columns>
                    <asp:CommandField ShowSelectButton="True" SelectText="Edit"/>
                </Columns>

            </asp:GridView>
        </div>

        <%--form input organisasi periode--%>
        <div id="divAddPeriodeOrganisasi" runat="server" visible="false">


            <div class="form-group">
                <label for="iKontak">Kontak</label>
                <input type="tel" maxlength="25" class="form-control" id="iKontak" name="iKontak" runat="server" aria-describedby="emailHelp" placeholder="Kontak" >
            </div>


            <div class="form-group">
                <label for="exampleInputEmail1">Instagram</label>
                <input type="text" maxlength="25" class="form-control" id="iInstagram" runat="server" aria-describedby="emailHelp" placeholder="Instagram">
            </div>

            <div class="form-group">
                <label for="exampleInputEmail1">Visi</label>
                <input type="text" class="form-control" maxlength="50" id="iVisi" runat="server" aria-describedby="emailHelp" placeholder="Visi">
            </div>

            <div class="form-group">
                <label for="exampleInputEmail1">Misi</label>
                <input type="text" class="form-control" maxlength="50" id="iMisi" runat="server" aria-describedby="emailHelp" placeholder="Misi">
            </div>

            <div class="form-group">
                <label for="exampleInputPst">Periode Input</label><br />
                <asp:Label ID="PeriodeShow" runat="server" Text="Label" Visible ="false"></asp:Label>
                <input type="text" class="form-control" id="iPeriode" runat="server" clientidmode="Static">
            </div>

            <div class="form-group">
                <label for="exampleInputEmail1">Email</label>
                <input type="email" maxlength="25" class="form-control" id="iEmail" runat="server" aria-describedby="emailHelp" placeholder="Email">
            </div>

            <div class="form-group">
                <label for="exampleInputAlamat">Alamat </label>
                <input type="text" class="form-control" maxlength="25" id="iAlamat" runat="server" aria-describedby="emailHelp" placeholder="Alamat">
            </div>

            <div class="form-group">
                <label for="file">Upload Foto Logo </label>
                <div class="col-sm-6">
                    <asp:FileUpload ID="ImgUploadOrganisasi" runat="server" />
                    <br />
                    <asp:Label ID="LblMessage" runat="server" Text="Label"></asp:Label>

                </div>
            </div>


            <div class="form-group">
                <label for="exampleInputDeskripsi">Deskripsi Organisasi </label>

                <input type="text" class="form-control" maxlength="50" id="iDesc" runat="server" aria-describedby="emailHelp" placeholder="Deskripsi Organisasi">
            </div>

            <div id="divBtSubmit" runat="server">            
                <asp:Button ID="btSubmitPeriodeOrganisasi" CssClass="btn btn-primary" runat="server" Text="Submit" OnClick="btSubmitPeriodeOrganisasi_Click" />
                
            </div>

            <div id="divBtUpdate" runat="server" >
                <asp:Button ID="BtUpdateOrganisasi" runat="server" OnClick="BtUpdateOrganisasi_Click" Text="Update" CssClass="btn btn-primary" />
            </div>
        </div>


        <%--button add new organisasi--%>
        <div visible="true" runat="server" id="divBtOrganisasiInput">
            <asp:Button ID="btShowOrganisasiInput" CssClass="btn btn-primary" runat="server" Text="Add Organisasi" OnClick="btShowOrganisasiInput_Click" />
        </div>
        <asp:DropDownList ID="DDPilihJenisOrganisasi" runat="server" AutoPostBack="true">
            <asp:ListItem Selected="True">Semua</asp:ListItem>
            <asp:ListItem>Fakultas</asp:ListItem>
            <asp:ListItem>Prodi</asp:ListItem>
            <asp:ListItem>Mandiri</asp:ListItem>
        </asp:DropDownList>
        <asp:HiddenField ID="hORGPeriodeUpdate" runat="server" />
        <div id="divViewOrganisasiList" runat="server" visible="true">
            <%--gridview organisasi(saja)--%>
            <asp:SqlDataSource
                ID="SqlOrganisasi"
                runat="server"
                ConnectionString='<%$ ConnectionStrings:SIKConnectionString %>'
                SelectCommand="SELECT * FROM ORGANISASI where ASAL_LEMBAGA = @jenis">
                <SelectParameters>
                    <asp:ControlParameter Name="jenis" ControlID="DDPilihJenisOrganisasi" PropertyName="SelectedValue" />
                </SelectParameters>

            </asp:SqlDataSource>


            <asp:GridView
                ID="gvOrganisasiList"
                CssClass="table"
                AutoGenerateColumns="false"
                DataKeyNames="KODE_ORGANISASI"
                AllowSorting="true"
                DataSourceID="SqlOrganisasi"
                OnSelectedIndexChanged="gvOrganisasiList_SelectedIndexChanged"
                runat="server">

                <Columns>
                    <%--tombol edit/select--%>
                    <asp:CommandField ShowSelectButton="true" SelectText="Edit" HeaderText="Action" />
                    <asp:BoundField DataField="KODE_ORGANISASI" HeaderText="Kode Organisasi" />
                    <asp:BoundField DataField="NAMA_ORGANISASI" HeaderText="Nama Organisasi" />
                    <asp:BoundField DataField="ASAL_LEMBAGA" HeaderText="Asal Lembaga" />
                </Columns>

            </asp:GridView>
        </div>

    </div>
</asp:Content>
