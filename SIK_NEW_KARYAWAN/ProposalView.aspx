﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterSite.Master" AutoEventWireup="true" CodeBehind="ProposalView.aspx.cs" Inherits="SIK_NEW_KARYAWAN.ProposalView" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="TopPage" runat="server">

    <div class="col-md-5">
    
    <div id="cPropview" runat="server" visible="true">
        <h1>View Proposal</h1>

    <asp:GridView ID="ProposalGridView" runat="server" AutoGenerateColumns="False" DataSourceID="ProposalListView" OnSelectedIndexChanged="ProposalGridView_SelectedIndexChanged" DataKeyNames="NO_SURAT_PROPOSAL">
        <Columns>
            <asp:CommandField ShowSelectButton="True" />
            <asp:BoundField DataField="NO_SURAT_PROPOSAL" HeaderText="NO_SURAT_PROPOSAL" ReadOnly="True" SortExpression="NO_SURAT_PROPOSAL" />
            <asp:BoundField DataField="JUDUL" HeaderText="JUDUL" SortExpression="JUDUL" />
            <asp:BoundField DataField="STATUS_TEMPAT" HeaderText="STATUS_TEMPAT" SortExpression="STATUS_TEMPAT" />
            <asp:BoundField DataField="TAHUN" HeaderText="TAHUN" SortExpression="TAHUN" />
            <asp:BoundField DataField="PENANGGUNG_JAWAB" HeaderText="PENANGGUNG_JAWAB" SortExpression="PENANGGUNG_JAWAB" />
            <asp:BoundField DataField="STATUS_PROPOSAL" HeaderText="STATUS_PROPOSAL" SortExpression="STATUS_PROPOSAL" />
            <asp:BoundField DataField="DATETIME_FROM" HeaderText="DATETIME_FROM" SortExpression="DATETIME_FROM" />
        </Columns>
    </asp:GridView>
        </div>


    <asp:SqlDataSource ID="ProposalListView" runat="server" ConnectionString="<%$ ConnectionStrings:SIKConnectionString %>" OnSelecting="SqlDataSource1_Selecting" SelectCommand="SELECT PRO.[NO_SURAT_PROPOSAL], JUDUL, STATUS_TEMPAT, TAHUN, [PENANGGUNG_JAWAB], STATUS_PROPOSAL, DATETIME_FROM
FROM  PROPOSAL PRO, PROPOSAL_WAKTU PROWAK WHERE PROWAK.NO_SURAT_PROPOSAL = PRO.NO_SURAT_PROPOSAL
ORDER BY DATETIME_FROM ASC"></asp:SqlDataSource>

        <asp:HiddenField ID="hNoSuratProp" runat="server" />

    <br />
        <div id="cPropDetail" runat="server" visible="false">
            <h1>Detail Proposal</h1>
        <asp:GridView ID="ProposalDetailGrid" runat="server" AutoGenerateColumns="False" DataSourceID="ProposalDetailSql">
            <Columns>
                <asp:BoundField DataField="JUDUL" HeaderText="JUDUL" SortExpression="JUDUL" />
                <asp:BoundField DataField="NO_SURAT_PROPOSAL" HeaderText="NO_SURAT_PROPOSAL" SortExpression="NO_SURAT_PROPOSAL" ReadOnly="True" />
                <asp:BoundField DataField="KODE_PROGRAM" HeaderText="KODE_PROGRAM" SortExpression="KODE_PROGRAM" />
                <asp:BoundField DataField="TANGGAL_TERIMA" HeaderText="TANGGAL_TERIMA" SortExpression="TANGGAL_TERIMA" />
                <asp:BoundField DataField="PENANGGUNG_JAWAB" HeaderText="PENANGGUNG_JAWAB" SortExpression="PENANGGUNG_JAWAB" />
                <asp:BoundField DataField="DANA_BANTUAN" HeaderText="DANA_BANTUAN" SortExpression="DANA_BANTUAN" />
                <asp:BoundField DataField="TAHUN" HeaderText="TAHUN" SortExpression="TAHUN" />
                <asp:BoundField DataField="TEMPAT" HeaderText="TEMPAT" SortExpression="TEMPAT" />
                <asp:BoundField DataField="DATETIME_FROM" HeaderText="DATETIME_FROM" SortExpression="DATETIME_FROM" />
                <asp:BoundField DataField="DATETIME_TO" HeaderText="DATETIME_TO" SortExpression="DATETIME_TO" />
            </Columns>
        </asp:GridView>
            
            <h2>Update Status Pada Proposal</h2>
        <asp:SqlDataSource ID="ProposalDetailSql" runat="server" ConnectionString="<%$ ConnectionStrings:SIKConnectionString %>" SelectCommand="SELECT [JUDUL], PRO.[NO_SURAT_PROPOSAL], [KODE_PROGRAM], [TANGGAL_TERIMA], [PENANGGUNG_JAWAB], [DANA_BANTUAN], [TAHUN], [TEMPAT], DATETIME_FROM, DATETIME_TO
FROM [PROPOSAL] PRO, PROPOSAL_WAKTU PROWAK WHERE (PRO.[NO_SURAT_PROPOSAL] = @NO_SURAT_PROPOSAL) AND PRO.NO_SURAT_PROPOSAL = PROWAK.NO_SURAT_PROPOSAL" OldValuesParameterFormatString="original_{0}">
            <SelectParameters>
                <asp:ControlParameter ControlID="hNoSuratProp" Name="NO_SURAT_PROPOSAL" PropertyName="Value" Type="String" />
            </SelectParameters>
            </asp:SqlDataSource>
            <br />
            <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" DataSourceID="PropGridUpdate">
                <Columns>
                    <asp:CommandField ShowEditButton="True" />
                    <asp:BoundField DataField="NO_SURAT_PROPOSAL" HeaderText="NO_SURAT_PROPOSAL" ReadOnly="True" SortExpression="NO_SURAT_PROPOSAL" />
                    <asp:CheckBoxField DataField="FISIK_IS_RECEIVED" HeaderText="FISIK_IS_RECEIVED" SortExpression="FISIK_IS_RECEIVED" />
                    <asp:CheckBoxField DataField="STATUS_PROPOSAL" HeaderText="STATUS_PROPOSAL" SortExpression="STATUS_PROPOSAL" />
                </Columns>
            </asp:GridView>
            <asp:SqlDataSource ID="PropGridUpdate" runat="server" ConflictDetection="CompareAllValues" ConnectionString="<%$ ConnectionStrings:SIKConnectionString %>" DeleteCommand="DELETE FROM [PROPOSAL] WHERE [NO_SURAT_PROPOSAL] = @original_NO_SURAT_PROPOSAL AND (([FISIK_IS_RECEIVED] = @original_FISIK_IS_RECEIVED) OR ([FISIK_IS_RECEIVED] IS NULL AND @original_FISIK_IS_RECEIVED IS NULL)) AND (([STATUS_PROPOSAL] = @original_STATUS_PROPOSAL) OR ([STATUS_PROPOSAL] IS NULL AND @original_STATUS_PROPOSAL IS NULL))" InsertCommand="INSERT INTO [PROPOSAL] ([NO_SURAT_PROPOSAL], [FISIK_IS_RECEIVED], [STATUS_PROPOSAL]) VALUES (@NO_SURAT_PROPOSAL, @FISIK_IS_RECEIVED, @STATUS_PROPOSAL)" OldValuesParameterFormatString="original_{0}" SelectCommand="SELECT [NO_SURAT_PROPOSAL], [FISIK_IS_RECEIVED], [STATUS_PROPOSAL] FROM [PROPOSAL] WHERE ([NO_SURAT_PROPOSAL] = @NO_SURAT_PROPOSAL)" UpdateCommand="UPDATE [PROPOSAL] SET [FISIK_IS_RECEIVED] = @FISIK_IS_RECEIVED, [STATUS_PROPOSAL] = @STATUS_PROPOSAL WHERE [NO_SURAT_PROPOSAL] = @original_NO_SURAT_PROPOSAL AND (([FISIK_IS_RECEIVED] = @original_FISIK_IS_RECEIVED) OR ([FISIK_IS_RECEIVED] IS NULL AND @original_FISIK_IS_RECEIVED IS NULL)) AND (([STATUS_PROPOSAL] = @original_STATUS_PROPOSAL) OR ([STATUS_PROPOSAL] IS NULL AND @original_STATUS_PROPOSAL IS NULL))">
                <DeleteParameters>
                    <asp:Parameter Name="original_NO_SURAT_PROPOSAL" Type="String" />
                    <asp:Parameter Name="original_FISIK_IS_RECEIVED" Type="Boolean" />
                    <asp:Parameter Name="original_STATUS_PROPOSAL" Type="Boolean" />
                </DeleteParameters>
                <InsertParameters>
                    <asp:Parameter Name="NO_SURAT_PROPOSAL" Type="String" />
                    <asp:Parameter Name="FISIK_IS_RECEIVED" Type="Boolean" />
                    <asp:Parameter Name="STATUS_PROPOSAL" Type="Boolean" />
                </InsertParameters>
                <SelectParameters>
                    <asp:ControlParameter ControlID="hNoSuratProp" Name="NO_SURAT_PROPOSAL" PropertyName="Value" Type="String" />
                </SelectParameters>
                <UpdateParameters>
                    <asp:Parameter Name="FISIK_IS_RECEIVED" Type="Boolean" />
                    <asp:Parameter Name="STATUS_PROPOSAL" Type="Boolean" />
                    <asp:Parameter Name="original_NO_SURAT_PROPOSAL" Type="String" />
                    <asp:Parameter Name="original_FISIK_IS_RECEIVED" Type="Boolean" />
                    <asp:Parameter Name="original_STATUS_PROPOSAL" Type="Boolean" />
                </UpdateParameters>
            </asp:SqlDataSource>
            <br />

            <div class="form-group">
                <label for="exampleInputEmail1">Alasan Penolakan:</label>
                <input type="text" class="form-control" id="ideksipsiisrejected" runat="server" aria-describedby="emailHelp" placeholder="Alasan" maxlength="50">
            </div>

            <br />
            Status Tempat:

            <br />

            <asp:DropDownList ID="DDStatusTempat" runat="server">
                <asp:ListItem>Ganti</asp:ListItem>
                <asp:ListItem>Diterima</asp:ListItem>
                <asp:ListItem Value="DiProses">Diproses</asp:ListItem>
                <asp:ListItem></asp:ListItem>
            </asp:DropDownList>
            <br />
            <br />
            <asp:Button ID="BtUpdateProp" runat="server" Text="Update " OnClick="BtUpdateProp_Click" />

            <br />

            

        </div>
    </div>
</asp:Content>
