﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterSite.Master" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="SIK_NEW_KARYAWAN.WebForm2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="TopPage" runat="server">

    <div class="col-md-6" style="padding-bottom: 20px">
        <form>

            <div class="form-group">
                <label for="exampleInputPassword1">NAMA</label>
                <input type="text" class="form-control" runat="server" id="iNama" placeholder="NAMA">
            </div>

            <asp:Button ID="btLogin" CssClass="btn btn-primary" runat="server" Text="Sign In" OnClick="btLogin_Click" />
            <asp:Button ID="btRegister" CssClass="btn btn-primary" runat="server" Text="Sign Up" OnClick="btRegister_Click" />


        </form>
    </div>

    <div id="divKaryawanList" runat="server" visible="true">
        <asp:SqlDataSource
            ID="SqlKaryawan"
            runat="server"
            ConnectionString='<%$ ConnectionStrings:SIKConnectionString %>' SelectCommand="SELECT * FROM KARYAWAN"></asp:SqlDataSource>
        <asp:GridView ID="gvKaryawanList" DataSourceID="SqlKaryawan" runat="server"></asp:GridView>
    </div>


</asp:Content>
