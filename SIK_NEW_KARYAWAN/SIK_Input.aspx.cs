﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using iTextSharp;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System.Diagnostics;
using System.IO;

namespace SIK_NEW_KARYAWAN
{
    public partial class SIK_Input : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            

        }

        public int RandomNumber(int min, int max)
        {
            Random random = new Random();
            return random.Next(min, max);
        }

        public string RandomString(int size, bool lowerCase)
        {
            StringBuilder builder = new StringBuilder();
            Random random = new Random();
            char ch;
            for (int i = 0; i < size; i++)
            {
                ch = Convert.ToChar(Convert.ToInt32(Math.Floor(26 * random.NextDouble() + 65)));
                builder.Append(ch);
            }
            if (lowerCase)
                return builder.ToString().ToLower();
            return builder.ToString();
        }

        public static string tmp = ""; //nama pdf yang udah di random

        protected void ButtonSubmitSIK_Click(object sender, EventArgs e)
        {
            

                    SIKTableAdapters.SIKTableAdapter ta = new SIKTableAdapters.SIKTableAdapter();
                    DateTime dtime = Convert.ToDateTime(DateTime.Now, System.Globalization.CultureInfo.GetCultureInfo("id-ID").DateTimeFormat);

                    ta.InsertSIK(DDProposal.SelectedValue, iPerihal.Value, dtime.ToString(), "-", iLampiran.Text, TTD.Value);

                    Response.Redirect(Request.RawUrl);

                
        }

        public static string pdfnomorSIK = "";

        protected void generatepdf_Click(object sender, EventArgs e)
        {
            Document dokumen = new Document(PageSize.A4, 25, 25, 30, 30);

            //font
            BaseFont bf = BaseFont.CreateFont(BaseFont.TIMES_ROMAN, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
            iTextSharp.text.Font font = new iTextSharp.text.Font(bf, 10, iTextSharp.text.Font.NORMAL);

            //save file
            PdfWriter writer = PdfWriter.GetInstance(dokumen, new System.IO.FileStream(Server.MapPath("new.pdf"), FileMode.Create));

            //PdfWriter.GetInstance(dokumen, new FileStream(Server.MapPath("new.pdf"), FileMode.Create));
            dokumen.Open();
            dokumen.Add(iTextSharp.text.Image.GetInstance(Server.MapPath("src/img/logo_untar.png")));
            //dokumen.Add(new Paragraph("test").SetAlignment(Element.ALIGN_CENTER));
            Paragraph judul = new Paragraph("SURAT IJIN KEGIATAN");
            judul.Alignment = Element.ALIGN_CENTER;
            dokumen.Add(judul);

           

            SIKTableAdapters.PROPOSALTableAdapter taprop = new SIKTableAdapters.PROPOSALTableAdapter();
            SIK.PROPOSALDataTable dt = taprop.SelectbyNoSurat(nmrsrt);

            SIKTableAdapters.PROPOSALTableAdapter tanoprop = new SIKTableAdapters.PROPOSALTableAdapter();
            SIK.PROPOSALDataTable dtnoporp = tanoprop.SelectNamaOrganByNoProp(nmrsrt); //select berdasarkan nama organ

            SIKTableAdapters.SIKTableAdapter taSIK = new SIKTableAdapters.SIKTableAdapter();
            SIK.SIKDataTable dtsik = taSIK.SelectByNmrPRP(nmrsrt);

            

            string nomorSIK = dtsik.Rows[0]["NOMOR_SIK"].ToString();
            string lampiranx = dtsik.Rows[0]["LAMPIRAN"].ToString();
            string NomorSuratPROP = dt.Rows[0]["NO_SURAT_PROPOSAL"].ToString();
            string KegiatanJudul = dtnoporp.Rows[0]["JUDUL_KEGIATAN"].ToString();
            string tanggalUpload = dt.Rows[0]["TANGGAL_TERIMA"].ToString();
            string namaorgan = dtnoporp.Rows[0]["NAMA_ORGANISASI"].ToString();
            string ketuapelaksanastrg = dtnoporp.Rows[0]["KETUA_PELAKSANA"].ToString();
            string penanggungjwb = dt.Rows[0]["PENANGGUNG_JAWAB"].ToString();
            string waktukegiatanmulai = dtnoporp.Rows[0]["DATETIME_FROM"].ToString();//gw gk tau ini mesti di loop apa gmn
            string waktukegiatanselesai = dtnoporp.Rows[0]["DATETIME_TO"].ToString(); //ini juga

            string tempat = dt.Rows[0]["TEMPAT"].ToString();
            //nomor surat
            Paragraph nomorsurat = new Paragraph("NOMOR:" + nomorSIK + " \n \n"); //hrsnya nmr SIK
            nomorsurat.Alignment = Element.ALIGN_CENTER;
            dokumen.Add(nomorsurat);

            Paragraph firstpara = new Paragraph("Setelah mempelajari surat UKM " + namaorgan + " nomor " + NomorSuratPROP + " tanggal " + tanggalUpload + ", dengan ini Direktur Kemahasiswaan dan alumni Universitas Tarumanagara memberikan ijin kepada UKM " + namaorgan + " Tarumanagara untuk mengikuti: \n \n")
            {

                //Paragraph firstpara = new Paragraph("Setelah mempelajari surat UKM #Kegiatan# nomor #nomor# tanggal #tanggal#, dengan ini Direktur Kemahasiswaan dan alumni Universitas Tarumanagara memberikan ijin kepada UKM #Kegiatan# Tarumanagara untuk mengikuti:LLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEE");

                Alignment = Element.ALIGN_JUSTIFIED
            };
            dokumen.Add(firstpara);


            Paragraph namaKegiatan = new Paragraph("Nama Kegiatan: " + KegiatanJudul)
            {
                Alignment = Element.ALIGN_LEFT
            };
            dokumen.Add(namaKegiatan);


            Paragraph penanggungJawab = new Paragraph("Penanggungjawab: " + penanggungjwb)
            {
                Alignment = Element.ALIGN_LEFT
            };
            dokumen.Add(penanggungJawab);

            Paragraph ketuaPelaksana = new Paragraph("ketua Pelaksana: " + ketuapelaksanastrg)
            {
                Alignment = Element.ALIGN_LEFT
            };
            dokumen.Add(ketuaPelaksana);


            Paragraph waktuKegiatan = new Paragraph("Waktu Kegiatan: " + waktukegiatanmulai + " - " + waktukegiatanselesai)
            {
                Alignment = Element.ALIGN_LEFT
            };
            dokumen.Add(waktuKegiatan);


            Paragraph tempatKegiatan = new Paragraph("Tempat: " + tempat + "\n \n")
            {
                Alignment = Element.ALIGN_LEFT
            };
            dokumen.Add(tempatKegiatan);

            Paragraph textbawahparg1 = new Paragraph("Demi menjaga nama baik Universitas Tarumanagara, harap kegiatan dilaksanakan dengan tertib sesuai dengan peraturan/ketentuan yang berlaku dan dengan penuh tanggungjawab serta tidak mengganggu hasil belajar. \n \n ")
            {
                Alignment = Element.ALIGN_JUSTIFIED


            };
            dokumen.Add(textbawahparg1);


            Paragraph textbawahparg2 = new Paragraph("Selanjutnya dalam waktu selambat-lambatnya 30 (tiga puluh) hari setelah selesainya kegiatan, penanggungjawab melaporkan pelaksanaan kegiatan dan pertanggungjawaban keuangan kepada Direktur Kemahasiswaan dan Alumni dengan melampirkan CD photo kegiatan. \n \n ")
            {
                Alignment = Element.ALIGN_JUSTIFIED

            };
            dokumen.Add(textbawahparg2);
            

            Paragraph textbawahparg3 = new Paragraph("Demikian Surat Izin Kegiatan ini diberikan untuk dipergunakan sesuai keperluan dengan catatan ijin kegiatan ini akan batal dengan sendirinya apabila ada ketentuan lain dari pihak berwajib. \n \n")
            {
                Alignment = Element.ALIGN_JUSTIFIED

            };
            dokumen.Add(textbawahparg3);

            Paragraph tandatgn = new Paragraph("Direktorat Kemahasiswaan dan Alumni \n \n")
            {
                Alignment = Element.ALIGN_JUSTIFIED

            };
            dokumen.Add(tandatgn);



            Paragraph texttanggal = new Paragraph(DateTime.Now.ToString("dd MMM yyyy" + "\n \n"))
            {
                Alignment = Element.ALIGN_LEFT

            };
            dokumen.Add(texttanggal);

            Paragraph lampiran = new Paragraph("Tembusan : \n" + lampiranx)
            {
                Alignment = Element.ALIGN_JUSTIFIED

            };
            dokumen.Add(lampiran);


            //dokumen.NewPage();
           // dokumen.Add(new Paragraph("new page"));

            dokumen.Close();


            //buka dokumen ke browser
            Process.Start(Server.MapPath("new.pdf"));
            Response.Redirect(Request.RawUrl);
        }

        public static string nmrsrt = "";

        protected void GridListSIK_SelectedIndexChanged(object sender, EventArgs e)
        {
            nmrsrt = (GridListSIK.SelectedRow.Cells[2].Text);
            pdfnomorSIK = (GridListSIK.SelectedRow.Cells[1].Text);
            Label2.Text = pdfnomorSIK;
            divSIKview.Visible = false;
            divInputSIK.Visible = false;
            UploadPDFjust.Visible = true;
            divgenerate.Visible = true;
            BtUpdateJust.Visible = true;
        }

        protected void BtInsertJust_Click(object sender, EventArgs e)
        {
            divSIKview.Visible = false;
            divInputSIK.Visible = true;
            divgenerate.Visible = false;
            BtSubmitSIKJUST.Visible = true;
        }

        protected void BtSubmitPDF_Click(object sender, EventArgs e)
        {
            if (PDFUploadSIK.HasFile)
            {
                SIKTableAdapters.SIKTableAdapter tasik = new SIKTableAdapters.SIKTableAdapter();
                SIK.SIKDataTable dtsik = tasik.DeletePDFSIKUpdate(pdfnomorSIK);

                string imgdelete = dtsik.Rows[0]["PDF_URL"].ToString();
                //BELOM
                //System.Diagnostics.Debug.WriteLine("test");
                string extensionPDF = System.IO.Path.GetExtension(PDFUploadSIK.FileName);

                StringBuilder builder = new StringBuilder();
                builder.Append(RandomString(4, true));
                builder.Append(RandomNumber(1000, 9999));
                builder.Append(RandomString(2, false));
                string imgrandom = builder.ToString(); //img random itu hash nama file 
                tmp = imgrandom + extensionPDF;

                if (extensionPDF.ToLower() != ".pdf")
                {
                    Label1.Text = "Tidak bisa upload karena harus .pdf";
                    return;
                }

                else if (extensionPDF.ToLower() == ".pdf")
                {

                    string imageFilePath = Server.MapPath("~/uploads/" + imgdelete);
                    File.Delete(imageFilePath);

                    PDFUploadSIK.SaveAs(MapPath("~/uploads/" + imgrandom + extensionPDF));
                    Label1.Text = "File succeed Uploaded";

                    SIKTableAdapters.SIKTableAdapter ta = new SIKTableAdapters.SIKTableAdapter();
                    ta.UpdateUploadPDF(tmp, pdfnomorSIK);

                    Response.Redirect(Request.RawUrl);

                }


            }

            else
            {
                Label1.Text = "file kosong";
                return;
            }

            Response.Redirect(Request.RawUrl);
        }

        protected void UploadPDFjust_Click(object sender, EventArgs e)
        {
            divuploadPDF.Visible = true;
            divSIKview.Visible = false;
            divInputSIK.Visible = false;
            divgenerate.Visible = false;
            UploadPDFjust.Visible = false;
            BtUpdateJust.Visible = false;
        }

        protected void BtUpdateJust_Click(object sender, EventArgs e)
        {
            SIKTableAdapters.SIKTableAdapter ta = new SIKTableAdapters.SIKTableAdapter();
            SIK.SIKDataTable dt = ta.SelectByNoSIK(Label2.Text); // label2 nomor siknya di aspx
            iPerihal.Value = dt.Rows[0]["PERIHAL"].ToString();
            TTD.Value = dt.Rows[0]["TTD"].ToString();
            iLampiran.Text = dt.Rows[0]["LAMPIRAN"].ToString();


            divuploadPDF.Visible = false;
            divSIKview.Visible = false;
            divInputSIK.Visible = true; 
            divgenerate.Visible = false;
            UploadPDFjust.Visible = false;
            divBtUpdateSIK.Visible = true;
            BtUpdateJust.Visible = false;
            divDDproposal.Visible = false;
               
        }

        protected void BtUpdateSIK_Click(object sender, EventArgs e)
        {
            SIKTableAdapters.SIKTableAdapter ta = new SIKTableAdapters.SIKTableAdapter();
            ta.UpdateSIK(iLampiran.Text, TTD.Value, iPerihal.Value, Label2.Text);

            Response.Redirect(Request.RawUrl);
        }
    }
}