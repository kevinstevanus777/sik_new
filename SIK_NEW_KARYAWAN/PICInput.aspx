﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterSite.Master" AutoEventWireup="true" CodeBehind="PICInput.aspx.cs" Inherits="SIK_NEW_KARYAWAN.WebForm8" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="TopPage" runat="server">


    <div class="col-md-5">
        <form>

            <%--algo ngeshow ganjil atau genap tahunnya--%>
            <asp:SqlDataSource
                ID="SqlMahasiswa"
                runat="server"
                ConnectionString='<%$ConnectionStrings:SIKConnectionString %>'
                SelectCommand="SELECT * FROM MAHASISWA"></asp:SqlDataSource>
            <label>NAMA MAHASISWA</label>
            <asp:DropDownList AutoPostBack="true" DataValueField="NIM" DataSourceID="SqlMahasiswa" DataTextField="NAMA" ID="oNim" runat="server" CssClass="form-control">
            </asp:DropDownList>

            <%--
                
                
            --%>
            <asp:SqlDataSource
                ID="SqlJabatan"
                runat="server"
                ConnectionString='<%$ ConnectionStrings:SIKConnectionString %>'
                SelectCommand="SELECT * FROM JABATAN"></asp:SqlDataSource>
            <label>JABATAN</label>
            <asp:DropDownList AutoPostBack="true" DataValueField="KODE_JABATAN" DataTextField="NAMA_JABATAN" DataSourceID="SqlJabatan" ID="oJabatan" runat="server" CssClass="form-control">
            </asp:DropDownList>

            <%--datasource organisasi--%>
            <asp:SqlDataSource
                ID="SqlOrganisasi"
                runat="server"
                ConnectionString='<%$ ConnectionStrings:SIKConnectionString %>'
                SelectCommand="SELECT * FROM ORGANISASI"></asp:SqlDataSource>
            <label>ORGANISASINYA</label>
            <asp:DropDownList OnTextChanged="oOrganisasi_TextChanged" AutoPostBack="true" DataValueField="KODE_ORGANISASI" DataTextField="NAMA_ORGANISASI" DataSourceID="SqlOrganisasi" ID="oOrganisasi" runat="server" CssClass="form-control">
            </asp:DropDownList>

            <%--datasource periode organisasinya--%>
            <asp:SqlDataSource
                ID="SqlPeriode"
                ConnectionString='<%$ ConnectionStrings:SIKConnectionString %>'
                SelectCommand="
                SELECT KODE_ORGANISASI,PERIODE,
                CASE 
	                WHEN PERIODE BETWEEN CONCAT(YEAR(CURRENT_TIMESTAMP),'/01/01') AND CONCAT(YEAR(CURRENT_TIMESTAMP),'/06/30') THEN CONCAT('GENAP/',YEAR(CURRENT_TIMESTAMP))
	                WHEN PERIODE BETWEEN CONCAT(YEAR(CURRENT_TIMESTAMP),'/07/01') AND CONCAT(YEAR(CURRENT_TIMESTAMP),'/12/31') THEN CONCAT('GANJIL/',YEAR(CURRENT_TIMESTAMP))
                END AS TAHUN_AKADEMIK
                FROM ORGANISASI_PERIODE
                WHERE KODE_ORGANISASI = @kode "
                runat="server">
                <SelectParameters>
                    <asp:ControlParameter ControlID="oOrganisasi" Name="kode" Type="String" PropertyName="SelectedValue" />
                </SelectParameters>
            </asp:SqlDataSource>

            <label>PERIODENYA</label>
            <asp:DropDownList
                ID="oPeriode"
                AutoPostBack="true"
                CssClass="form-control"
                DataSourceID="SqlPeriode"
                DataValueField="PERIODE"
                DataTextField="TAHUN_AKADEMIK"
                runat="server">
            </asp:DropDownList>

            <%--algo ngeshow ganjil atau genap tahunnya--%>
            <%--
                
                
            --%>


            <asp:Button ID="btSubmitPIC" runat="server" CssClass="btn btn-primary" OnClick="btSubmitPIC_Click" Text="Submit" />

            <br />
            <br />
            <asp:GridView ID="GvPIC" runat="server" AutoGenerateColumns="False" DataSourceID="SqlDataSource1">
                <Columns>
                    <asp:BoundField DataField="NIM" HeaderText="NIM" SortExpression="NIM" />
                    <asp:BoundField DataField="KODE_JABATAN" HeaderText="KODE_JABATAN" SortExpression="KODE_JABATAN" />
                    <asp:BoundField DataField="PERIODE" HeaderText="PERIODE" SortExpression="PERIODE" />
                    <asp:BoundField DataField="KODE_ORGANISASI" HeaderText="KODE_ORGANISASI" SortExpression="KODE_ORGANISASI" />
                </Columns>
            </asp:GridView>
            <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:SIKConnectionString %>" SelectCommand="SELECT [NIM], [KODE_JABATAN], [PERIODE], [KODE_ORGANISASI] FROM [PIC]"></asp:SqlDataSource>
            <br />

        </form>
    </div>



</asp:Content>
