﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SIK_NEW_KARYAWAN
{
    public partial class WebForm3 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["NIK"] == null)
            {
                Response.Redirect("Login.aspx");
            }
        }

        protected void btSubmitFakultas_Click(object sender, EventArgs e)
        {


            SIKTableAdapters.FAKULTASTableAdapter ta = new SIKTableAdapters.FAKULTASTableAdapter();
            ta.InsertFakultas(iFakultas.Value);

            ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('Data Berhasil Masuk ke DB');", true);
            Response.Redirect(Request.RawUrl);

        }

        
    }
}