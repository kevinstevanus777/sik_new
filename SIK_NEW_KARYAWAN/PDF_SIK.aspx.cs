﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using iTextSharp;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System.Diagnostics;
using System.IO;



namespace SIK_NEW_KARYAWAN
{
    public partial class WebForm7 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }
        //references
        //https://www.c-sharpcorner.com/uploadfile/f2e803/basic-pdf-creation-using-itextsharp-part-i/
        //    https://www.c-sharpcorner.com/uploadfile/f2e803/basic-pdf-creation-using-itextsharp-part-ii/
        //https://www.c-sharpcorner.com/uploadfile/f2e803/basic-pdf-creation-using-itextsharp-part-iii/

        protected void btGenerate_Click(object sender, EventArgs e)
        {
            //using (MemoryStream ms = new MemoryStream())
            //{
            //    Document document = new Document(PageSize.A4, 25, 25, 30, 30);
            //    PdfWriter writer = PdfWriter.GetInstance(document, ms);
            //    document.Open();
            //    document.Add(new Paragraph("Hello World"));

            //    document.Close();
            //    writer.Close();
            //    Response.ContentType = "pdf/application";
            //    Response.AddHeader("content-disposition",
            //    "attachment;filename=First PDF document.pdf");
            //    Response.OutputStream.Write(ms.GetBuffer(), 0, ms.GetBuffer().Length);
            //}


            //NOTE!!!: kalo yang dibawah ini bakal nge open pdf nya di browser, kalo cara diatas
            //buat dokument baru ; dengan ukuran a4, beserta marginnya(25,25,30,30);
            Document dokumen = new Document(PageSize.A4, 25, 25, 30, 30);

            //font
            BaseFont bf = BaseFont.CreateFont(BaseFont.TIMES_ROMAN, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
            iTextSharp.text.Font font = new iTextSharp.text.Font(bf, 10, iTextSharp.text.Font.NORMAL);

            //save file
            PdfWriter writer = PdfWriter.GetInstance(dokumen, new FileStream(Server.MapPath("new.pdf"), FileMode.Create));

            //PdfWriter.GetInstance(dokumen, new FileStream(Server.MapPath("new.pdf"), FileMode.Create));
            dokumen.Open();
            dokumen.Add(iTextSharp.text.Image.GetInstance(Server.MapPath("src/img/logo_untar.png")));
            //dokumen.Add(new Paragraph("test").SetAlignment(Element.ALIGN_CENTER));
            Paragraph judul = new Paragraph("SURAT IJIN KEGIATAN");
            judul.Alignment = Element.ALIGN_CENTER;
            dokumen.Add(judul);


            //nomor surat
            Paragraph nomorsurat = new Paragraph("NOMOR SURAT");
            nomorsurat.Alignment = Element.ALIGN_CENTER;
            dokumen.Add(nomorsurat);

            Paragraph firstpara = new Paragraph(new Chunk("Setelah mempelajari surat UKM #Kegiatan# nomor #nomor# tanggal #tanggal#, dengan ini Direktur Kemahasiswaan dan alumni Universitas Tarumanagara memberikan ijin kepada UKM #Kegiatan# Tarumanagara untuk mengikuti:", font))
            {

                //Paragraph firstpara = new Paragraph("Setelah mempelajari surat UKM #Kegiatan# nomor #nomor# tanggal #tanggal#, dengan ini Direktur Kemahasiswaan dan alumni Universitas Tarumanagara memberikan ijin kepada UKM #Kegiatan# Tarumanagara untuk mengikuti:LLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLLEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEE");

                Alignment = Element.ALIGN_JUSTIFIED
            };
            dokumen.Add(firstpara);


            Paragraph namaKegiatan = new Paragraph("Nama Kegiatan: ")
            {
                Alignment = Element.ALIGN_LEFT
            };
            dokumen.Add(namaKegiatan);


            Paragraph penanggungJawab = new Paragraph("Penanggungjawab: ")
            {
                Alignment = Element.ALIGN_LEFT
            };
            dokumen.Add(penanggungJawab);

            Paragraph ketuaPelaksana = new Paragraph("ketua Pelaksana: ")
            {
                Alignment = Element.ALIGN_LEFT
            };
            dokumen.Add(ketuaPelaksana);


            Paragraph waktuKegiatan = new Paragraph("Waktu Kegiatan: ")
            {
                Alignment = Element.ALIGN_LEFT
            };
            dokumen.Add(waktuKegiatan);


            Paragraph tempatKegiatan = new Paragraph("Tempat: ")
            {
                Alignment = Element.ALIGN_LEFT
            };
            dokumen.Add(tempatKegiatan);

            Paragraph textbawahparg1 = new Paragraph("Demi menjaga nama baik Universitas Tarumanagara, harap kegiatan dilaksanakan dengan tertib sesuai dengan peraturan/ketentuan yang berlaku dan dengan penuh tanggungjawab serta tidak mengganggu hasil belajar. ")
            {
                Alignment = Element.ALIGN_JUSTIFIED
            };
            dokumen.Add(textbawahparg1);


            Paragraph textbawahparg2 = new Paragraph("Selanjutnya dalam waktu selambat-lambatnya 30 (tiga puluh) hari setelah selesainya kegiatan, penanggungjawab melaporkan pelaksanaan kegiatan dan pertanggungjawaban keuangan kepada Direktur Kemahasiswaan dan Alumni dengan melampirkan CD photo kegiatan.")
            {
                Alignment = Element.ALIGN_JUSTIFIED

            };
            dokumen.Add(textbawahparg2);

            Paragraph textbawahparg3 = new Paragraph(" Demikian Surat Izin Kegiatan ini diberikan untuk dipergunakan sesuai keperluan dengan catatan ijin kegiatan ini akan batal dengan sendirinya apabila ada ketentuan lain dari pihak berwajib.")
            {
                Alignment = Element.ALIGN_JUSTIFIED

            };
            dokumen.Add(textbawahparg3);


            //PdfContentByte cb = writer.DirectContent;

            //cb.BeginText();

            ////set font
            //BaseFont f_cn = BaseFont.CreateFont("c:\\windows\\fonts\\calibri.ttf", BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
            //cb.SetFontAndSize(f_cn, 9);
            //cb.SetTextMatrix(0, 0);



            ////logo untar
            //iTextSharp.text.Image img = iTextSharp.text.Image.GetInstance(Server.MapPath("src/img/logo_untar.png"));
            //img.SetAbsolutePosition(30,750);
            //img.ScalePercent(50);
            //cb.AddImage(img);





            /*            cb.ShowText("Hello world")*/
            //int row = 1;
            //for (int y = 0; y != 70; y++)
            //{
            //    cb.SetTextMatrix(10, row);
            //    cb.ShowText("Y: " + row.ToString());
            //    row += 12; // The spacing between the rows is set to 12 "points"  
            //}
            //int col = 35;
            //for (int x = 0; x != 22; x++)
            //{
            //    cb.SetTextMatrix(col, 829);
            //    cb.ShowText("X: " + col.ToString());
            //    col += 25; // The spacing between the columns is set to 25 "points"  
            //}
            //cb.EndText();
            //dokumen.Add(new Paragraph("hello world"));
            dokumen.NewPage();
            dokumen.Add(new Paragraph("new page"));

            dokumen.Close();


            //buka dokumen ke browser
            Process.Start(Server.MapPath("new.pdf"));

        }

        private void writeText(PdfContentByte cb, string Text, int X, int Y, BaseFont font, int Size)
        {
            cb.SetFontAndSize(font, Size);
            cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, Text, X, Y, 0);
        }

        protected void BtSiksubmit_Click(object sender, EventArgs e)
        {
            //SIKTableAdapters.PRODITableAdapter ta = new SIKTableAdapters.PRODITableAdapter();
           // ta.InsertProdi(oFakultas.SelectedItem.Value, iProdi.Value);

           // SIKTableAdapters.SIKTableAdapter ta = new SIKTableAdapters.SIKTableAdapter();
            //ta.InsertSIK(ddSuratProposal.SelectedItem.Value, iPerihal.Value, tglSrt.Value);

        }
    }
}