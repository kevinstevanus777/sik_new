﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;


namespace SIK_NEW_KARYAWAN
{
    public partial class WebForm5 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["NIK"] == null)
            {
                Response.Redirect("Login.aspx");
            }
        }

        protected void btAddOrganisasi_Click(object sender, EventArgs e)
        {
            //buat table adapternya


            //jika prodi
            if (DDJenisOrganisasi.SelectedValue == "Fakultas")
            {
                SIKTableAdapters.ORGANISASITableAdapter orgTA = new SIKTableAdapters.ORGANISASITableAdapter();
                orgTA.InsertOrganisasi(iNamaOrganisasi.Value, "FAKULTAS");


                //get lastincrement from tabel utama
                SIK.ORGANISASIDataTable dt = orgTA.SelectLastKodeOrganisasi();
                SIK.ORGANISASIRow da = (SIK.ORGANISASIRow)dt.Rows[0];

                string lastNum = da.KODE_ORGANISASI;
                System.Diagnostics.Debug.WriteLine(lastNum);

                //insert to child table
                SIKTableAdapters.ORGANISASI_FAKULTASTableAdapter tafakultas = new SIKTableAdapters.ORGANISASI_FAKULTASTableAdapter();
                tafakultas.InsertOrganisasiFakultas(lastNum, oFakultas.SelectedValue);

                ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('Data Berhasil Masuk ke DB');", true);

                Response.Redirect(Request.RawUrl);
            }

            //jika fakultas
            else if (DDJenisOrganisasi.SelectedValue == "Prodi")
            {
                SIKTableAdapters.ORGANISASITableAdapter orgTA = new SIKTableAdapters.ORGANISASITableAdapter();
                orgTA.InsertOrganisasi(iNamaOrganisasi.Value, "PRODI");

                //get lastincrement from tabel utama
                SIK.ORGANISASIDataTable dt = orgTA.SelectLastKodeOrganisasi();
                SIK.ORGANISASIRow da = (SIK.ORGANISASIRow)dt.Rows[0];

                string lastNum = da.KODE_ORGANISASI;
                System.Diagnostics.Debug.WriteLine(lastNum);

                //insert to child table
                SIKTableAdapters.ORGANISASI_PRODITableAdapter taprodi = new SIKTableAdapters.ORGANISASI_PRODITableAdapter();
                taprodi.InsertOrganisasiProdi(lastNum, oProdi.SelectedValue);
                ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('Data Berhasil Masuk ke DB');", true);

                Response.Redirect(Request.RawUrl);


            }
            //jika mandiri
            else if (DDJenisOrganisasi.SelectedValue == "Mandiri")
            {
                SIKTableAdapters.ORGANISASITableAdapter orgTA = new SIKTableAdapters.ORGANISASITableAdapter();
                orgTA.InsertOrganisasi(iNamaOrganisasi.Value, "MANDIRI");

                ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('Data Berhasil Masuk ke DB');", true);

                Response.Redirect(Request.RawUrl);

            }

            //orgTA.InsertOrganisasi()
            //SIKTableAdapters.PRODITableAdapter ta = new SIKTableAdapters.PRODITableAdapter();
            //ta.InsertProdi(oFakultas.SelectedItem.Value, iProdi.Value);
        }

        protected void btShowOrganisasiInput_Click(object sender, EventArgs e)
        {
            divOrganisasiInput.Visible = true;
        }

        protected void DDJenisOrganisasi_TextChanged(object sender, EventArgs e)
        {
            if (DDJenisOrganisasi.SelectedValue == "Fakultas")
            {
                divFakultas.Visible = true;
                divProdi.Visible = false;

            }
            else if (DDJenisOrganisasi.SelectedValue == "Prodi")
            {
                divProdi.Visible = true;
                divFakultas.Visible = true;
            }
            else if (DDJenisOrganisasi.SelectedValue == "Mandiri")
            {
                divProdi.Visible = false;
                divFakultas.Visible = false;
            }
        }
        //public static string _organisasiID;
        protected void gvOrganisasiList_SelectedIndexChanged(object sender, EventArgs e)
        {
            string organisasiID = (gvOrganisasiList.SelectedValue).ToString();
            hKodeOrganisasi.Value = organisasiID;
            divAddPeriodeOrganisasi.Visible = false;
            divViewListPeriodeOrganisasi.Visible = true;
            divBtOrganisasiInput.Visible = false;
            divViewOrganisasiList.Visible = false;
            oKodeOrganisasi.InnerText = organisasiID;
            divBtUpdate.Visible = false;

        }


        protected void btAddPeriode_Click(object sender, EventArgs e)
        {
            divAddPeriodeOrganisasi.Visible = true;
        }
        //randomizer
        public int RandomNumber(int min, int max)
        {
            Random random = new Random();
            return random.Next(min, max);
        }

        public string RandomString(int size, bool lowerCase)
        {
            StringBuilder builder = new StringBuilder();
            Random random = new Random();
            char ch;
            for (int i = 0; i < size; i++)
            {
                ch = Convert.ToChar(Convert.ToInt32(Math.Floor(26 * random.NextDouble() + 65)));
                builder.Append(ch);
            }
            if (lowerCase)
                return builder.ToString().ToLower();
            return builder.ToString();
        }

        public static string tmp = ""; //nama image yang udah di random

        protected void btSubmitPeriodeOrganisasi_Click(object sender, EventArgs e)
        {
            //untuk upload Image di Organisasi Periode

            

            if (ImgUploadOrganisasi.HasFiles)
            {
                string pictureextension = System.IO.Path.GetExtension(ImgUploadOrganisasi.FileName);

                StringBuilder builder = new StringBuilder();
                builder.Append(RandomString(4, true));
                builder.Append(RandomNumber(1000, 9999));
                builder.Append(RandomString(2, false));
                string imgrandom = builder.ToString();
                tmp = imgrandom + pictureextension;

                if (pictureextension.ToLower() != ".jpg" && pictureextension.ToLower() != ".png")
                {
                    LblMessage.Text = "Tidak bisa upload karena harus .jpg dan .png";
                    return;
                }
                else
                {
                    int filesize = ImgUploadOrganisasi.PostedFile.ContentLength;

                    if (filesize > 2000000)
                    {
                        LblMessage.Text = "file terlalu besar";
                        return;
                    }
                    else
                    {
                        //string panteq = ImgUploadOrganisasi.FileName;
                        //System.Text.RegularExpressions.Regex r = new System.Text.RegularExpressions.Regex(@"\\.");

                        //string[] exploded = panteq.TrimStart('.').Split('.');


                        //System.Diagnostics.Debug.WriteLine(exploded);

                        //string Temp = ImgUploadOrganisasi.FileName
                        
                        
                        ImgUploadOrganisasi.SaveAs(Server.MapPath("~/uploads/" + imgrandom + pictureextension));
                        LblMessage.Text = "FIle Uploaded";

                        string organisasiID = (gvOrganisasiList.SelectedValue).ToString();
                        SIKTableAdapters.ORGANISASI_PERIODETableAdapter ta = new SIKTableAdapters.ORGANISASI_PERIODETableAdapter();
                        ta.InsertOrganisasiPeriode(organisasiID, iKontak.Value, iInstagram.Value, iVisi.Value, iPeriode.Value, iMisi.Value, iEmail.Value, iAlamat.Value, tmp, iDesc.Value);

                    }
                }

            }

            else
            {
                LblMessage.Text = "file kosong";
                return;
            }

            if (iPeriode.Value == "")
            {
                Response.Write("<script language='javascript'>window.alert('periode belom diisi, refreshing..');</script>");
                return;
            }

            


            Response.Write("<script language='javascript'>window.alert('Data berhasil terinput');window.location='ProdiInput.aspx';</script>");
            Response.Redirect(Request.RawUrl);



        }

        public static string imgdelete = "";
        
        protected void gvListPeriodeList_SelectedIndexChanged(object sender, EventArgs e)
        {
            //UPDATE ORGANISASI
            string PeriodeUpdate = gvListPeriodeList.SelectedRow.Cells[5].Text;
            string organisasiID = (gvOrganisasiList.SelectedValue).ToString();
            hKodeOrganisasi.Value = organisasiID;
            hORGPeriodeUpdate.Value = PeriodeUpdate;

            //label periode doang
            PeriodeShow.Text = hORGPeriodeUpdate.Value;

            PeriodeShow.Visible = true;
            divAddPeriodeOrganisasi.Visible = true;            
            divViewListPeriodeOrganisasi.Visible = false;
            divBtOrganisasiInput.Visible = false;
            divViewOrganisasiList.Visible = false;
            divBtSubmit.Visible = false;
            iPeriode.Visible = false;
            divBtUpdate.Visible = true;

            

            //DAPETIN VALUE
            SIKTableAdapters.ORGANISASI_PERIODETableAdapter taorg = new SIKTableAdapters.ORGANISASI_PERIODETableAdapter();

            SIK.ORGANISASI_PERIODEDataTable dt = taorg.SelectbyKodeOrgAndPeriode(organisasiID , PeriodeUpdate);

            //ISI VALUE DI TEXTBOX
            iKontak.Value = dt.Rows[0]["KONTAK"].ToString();
            iInstagram.Value = dt.Rows[0]["INSTAGRAM"].ToString();
            iVisi.Value = dt.Rows[0]["VISI"].ToString();
            iPeriode.Value = dt.Rows[0]["PERIODE"].ToString();
            iMisi.Value = dt.Rows[0]["MISI"].ToString();
            iEmail.Value = dt.Rows[0]["EMAIL"].ToString();
            iAlamat.Value = dt.Rows[0]["ALAMAT"].ToString();
            iDesc.Value = dt.Rows[0]["DESKRIPSI_ORGANISASI"].ToString();
            imgdelete = dt.Rows[0]["LOGO_ORGANISASI"].ToString();


        }

        protected void BtUpdateOrganisasi_Click(object sender, EventArgs e)
        {

            if (ImgUploadOrganisasi.HasFiles)
            {
                string pictureextension = System.IO.Path.GetExtension(ImgUploadOrganisasi.FileName);

                StringBuilder builder = new StringBuilder();
                builder.Append(RandomString(4, true));
                builder.Append(RandomNumber(1000, 9999));
                builder.Append(RandomString(2, false));
                string imgrandom = builder.ToString();
                tmp = imgrandom + pictureextension;

                if (pictureextension.ToLower() != ".jpg" && pictureextension.ToLower() != ".png")
                {
                    LblMessage.Text = "Tidak bisa upload karena harus .jpg dan .png";
                    return;
                }
                else
                {
                    int filesize = ImgUploadOrganisasi.PostedFile.ContentLength;

                    if (filesize > 2000000)
                    {
                        LblMessage.Text = "file terlalu besar";
                        return;
                    }
                    else
                    {
                        //string panteq = ImgUploadOrganisasi.FileName;
                        //System.Text.RegularExpressions.Regex r = new System.Text.RegularExpressions.Regex(@"\\.");

                        //string[] exploded = panteq.TrimStart('.').Split('.');


                        //System.Diagnostics.Debug.WriteLine(exploded);

                        //string Temp = ImgUploadOrganisasi.FileName
                        System.Diagnostics.Debug.WriteLine(imgdelete);

                        
                        string imageFilePath = Server.MapPath("~/uploads/" + imgdelete);                        
                        System.IO.File.Delete(imageFilePath);


                        ImgUploadOrganisasi.SaveAs(Server.MapPath("~/uploads/" + imgrandom + pictureextension));
                        LblMessage.Text = "FIle Uploaded";

                        SIKTableAdapters.ORGANISASI_PERIODETableAdapter ta = new SIKTableAdapters.ORGANISASI_PERIODETableAdapter();
                        ta.UpdateOrganisasi(iKontak.Value, iInstagram.Value, iVisi.Value, iMisi.Value, iEmail.Value, iAlamat.Value, tmp, iDesc.Value, hKodeOrganisasi.Value, hORGPeriodeUpdate.Value);
                        Response.Redirect(Request.RawUrl);

                    }
                }

            }

            else
            {
                SIKTableAdapters.ORGANISASI_PERIODETableAdapter ta = new SIKTableAdapters.ORGANISASI_PERIODETableAdapter();
                ta.UpdateOrganTanpaGambar(iKontak.Value, iInstagram.Value, iVisi.Value, iMisi.Value, iEmail.Value, iAlamat.Value, iDesc.Value, hKodeOrganisasi.Value, hORGPeriodeUpdate.Value);
                Response.Redirect(Request.RawUrl);
            }

            
        }
    }
}