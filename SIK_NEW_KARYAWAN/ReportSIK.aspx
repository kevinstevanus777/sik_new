﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterSite.Master" AutoEventWireup="true" CodeBehind="ReportSIK.aspx.cs" Inherits="SIK_NEW_KARYAWAN.ReportSIK" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="TopPage" runat="server">
    <h1>Report Surat Izin Kegiatan</h1>
    <asp:GridView ID="GridView1" runat="server" AllowPaging="True" AllowSorting="True" AutoGenerateColumns="False" DataSourceID="SqlDataSource1">
        <Columns>
            <asp:BoundField DataField="NO_SURAT_PROPOSAL" HeaderText="NO_SURAT_PROPOSAL" ReadOnly="True" SortExpression="NO_SURAT_PROPOSAL" />
            <asp:BoundField DataField="TANGGAL_TERIMA" HeaderText="TANGGAL_TERIMA" SortExpression="TANGGAL_TERIMA" />
            <asp:BoundField DataField="NOMOR_SIK" HeaderText="NOMOR_SIK" ReadOnly="True" SortExpression="NOMOR_SIK" />
            <asp:BoundField DataField="TANGGAL_SURAT" HeaderText="TANGGAL_SURAT" SortExpression="TANGGAL_SURAT" />
            <asp:BoundField DataField="PELAKSANAAN_START" HeaderText="PELAKSANAAN_START" SortExpression="PELAKSANAAN_START" />
            <asp:BoundField DataField="PELAKSANAAN_FINISH" HeaderText="PELAKSANAAN_FINISH" SortExpression="PELAKSANAAN_FINISH" />
            <asp:BoundField DataField="TEMPAT" HeaderText="TEMPAT" SortExpression="TEMPAT" />
            <asp:BoundField DataField="DANA" HeaderText="DANA" SortExpression="DANA" />
            <asp:BoundField DataField="DANA_BANTUAN" HeaderText="DANA_BANTUAN" SortExpression="DANA_BANTUAN" />
        </Columns>
    </asp:GridView>

    <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:SIKConnectionString %>" SelectCommand="SELECT P.NO_SURAT_PROPOSAL, TANGGAL_TERIMA , NOMOR_SIK, TANGGAL_SURAT, PELAKSANAAN_START, PELAKSANAAN_FINISH, TEMPAT, DANA, DANA_BANTUAN
FROM PROPOSAL P, SIK S, PROGRAM_KERJA PK
WHERE P.NO_SURAT_PROPOSAL = S.NO_SURAT_PROPOSAL AND PK.KODE_PROGRAM= P.KODE_PROGRAM"></asp:SqlDataSource>

    <br />

   

</asp:Content>
