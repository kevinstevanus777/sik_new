﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SIK_NEW_KARYAWAN
{
    public partial class LPJ_view : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["NIK"] == null)
            {
                Response.Redirect("Login.aspx");
            }

            SIKTableAdapters.KARYAWANTableAdapter takry = new SIKTableAdapters.KARYAWANTableAdapter();
            SIK.KARYAWANDataTable dt = takry.Selectsession(Session["NIK"].ToString());
            LblNamaKaryawan.Text= dt.Rows[0]["NAMA_KARYAWAN"].ToString();

            HKodeNIK.Value = dt.Rows[0]["NIK"].ToString();
        }

        protected void LPJGridView_SelectedIndexChanged(object sender, EventArgs e)
        {
            divviewLPJ.Visible = false;
            divLPJ_update.Visible = true;
            string nolpj= (LPJGridView.SelectedValue).ToString();
            hNoSuratLPJ.Value = nolpj;
            SIKTableAdapters.LPJTableAdapter taprop = new SIKTableAdapters.LPJTableAdapter();
            SIK.LPJDataTable dt = taprop.SelectAlasanByNoLPJ(nolpj);

            ialasan.Value = dt.Rows[0]["ALASAN"].ToString();


            //string NAMAKRY = dt.Rows[0]["NAMA_KARYAWAN"].ToString();
            // SIKTableAdapters.KARYAWANTableAdapter ta = new SIKTableAdapters.KARYAWANTableAdapter();
            // SIK.KARYAWANDataTable dw = ta.SELECTBYNAMA(NAMAKRY);

            SIKTableAdapters.LPJTableAdapter ta = new SIKTableAdapters.LPJTableAdapter();
            ta.UpdateNIK(HKodeNIK.Value, hNoSuratLPJ.Value);


        }

        protected void LPJListView_Selecting(object sender, SqlDataSourceSelectingEventArgs e)
        {

        }

        protected void BtupdateAlasan_Click(object sender, EventArgs e)
        {
            SIKTableAdapters.LPJTableAdapter talpj = new SIKTableAdapters.LPJTableAdapter();
            talpj.UpdateAlasanLPJ(ialasan.Value, hNoSuratLPJ.Value);
            Response.Redirect(Request.RawUrl);
        }
    }
}