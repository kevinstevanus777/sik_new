﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterSite.Master" AutoEventWireup="true" CodeBehind="ProgramKerjaview.aspx.cs" Inherits="SIK_NEW_KARYAWAN.ProgramKerjaInput" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="TopPage" runat="server">
        <h1>Program Kerja</h1>

    <div id="cProkerView" runat="server" visible="true">
    <asp:GridView ID="GVProgramKerjalist" 
        CssClass="table"
        AutoGenerateColumns="False" 
        runat="server" 
        OnSelectedIndexChanged="GVProgramKerjalist_SelectedIndexChanged" 
        AllowSorting="True" 
        DataSourceID="ViewProker" DataKeyNames="KODE_PROGRAM">

        <Columns>
            <asp:CommandField ShowSelectButton="True" />
            <asp:BoundField DataField="KODE_PROGRAM" HeaderText="KODE_PROGRAM" SortExpression="KODE_PROGRAM" ReadOnly="True" />
            <asp:BoundField DataField="NAMA_ORGANISASI" HeaderText="NAMA_ORGANISASI" SortExpression="NAMA_ORGANISASI" />
            <asp:BoundField DataField="JUDUL_KEGIATAN" HeaderText="JUDUL_KEGIATAN" SortExpression="JUDUL_KEGIATAN" />
            <asp:BoundField DataField="PERIODE" HeaderText="PERIODE" SortExpression="PERIODE" />
        </Columns>


    </asp:GridView>
    
        <asp:SqlDataSource ID="ViewProker" runat="server" OnSelecting="SqlDataSource1_Selecting" ConnectionString="<%$ ConnectionStrings:SIKConnectionString %>" SelectCommand="SELECT PROGRAM_KERJA.KODE_PROGRAM, ORGANISASI.NAMA_ORGANISASI, PROGRAM_KERJA.JUDUL_KEGIATAN, PROGRAM_KERJA.PERIODE FROM PROGRAM_KERJA INNER JOIN ORGANISASI ON PROGRAM_KERJA.KODE_ORGANISASI = ORGANISASI.KODE_ORGANISASI">
         </asp:SqlDataSource>
     
        <br />
        <br />
        <br />
        </div>
         <div id="cProkerDetail" runat="server" visible="false">
             Program Kerja Detail
             <br />
            
        <asp:GridView ID="ProkerDetail" runat="server" AutoGenerateColumns="False" DataSourceID="DetailProker" OnSelectedIndexChanged="ProkerDetail_SelectedIndexChanged">
            <Columns>
            
                <asp:BoundField DataField="KODE_PROGRAM" HeaderText="KODE_PROGRAM" ReadOnly="True" SortExpression="KODE_PROGRAM" Visible="False" />
                <asp:BoundField DataField="KODE_ORGANISASI" HeaderText="KODE_ORGANISASI" SortExpression="KODE_ORGANISASI" />
                <asp:BoundField DataField="PERIODE" HeaderText="PERIODE" SortExpression="PERIODE" />
                <asp:BoundField DataField="JUDUL_KEGIATAN" HeaderText="JUDUL_KEGIATAN" SortExpression="JUDUL_KEGIATAN" />
                <asp:BoundField DataField="PELAKSANAAN_START" HeaderText="PELAKSANAAN_START" SortExpression="PELAKSANAAN_START" />
                <asp:BoundField DataField="PELAKSANAAN_FINISH" HeaderText="PELAKSANAAN_FINISH" SortExpression="PELAKSANAAN_FINISH" />
                <asp:BoundField DataField="PDF_URL" HeaderText="PDF_URL" SortExpression="PDF_URL" />
                <asp:BoundField DataField="KETUA_PELAKSANA" HeaderText="KETUA_PELAKSANA" SortExpression="KETUA_PELAKSANA" />
                <asp:BoundField DataField="DESKRIPSI_PROG" HeaderText="DESKRIPSI_PROG" SortExpression="DESKRIPSI_PROG" />
                <asp:BoundField DataField="DANA" HeaderText="DANA" SortExpression="DANA" />
                <asp:BoundField DataField="KETERANGAN_PESERTA" HeaderText="KETERANGAN_PESERTA" SortExpression="KETERANGAN_PESERTA" />
            </Columns>
        </asp:GridView>

        <asp:HiddenField ID="hKodeProker" runat="server" />
        <br />

        <asp:SqlDataSource ID="DetailProker" runat="server" ConnectionString="<%$ ConnectionStrings:SIKConnectionString %>" SelectCommand="SELECT KODE_PROGRAM, KODE_ORGANISASI, PERIODE, JUDUL_KEGIATAN, PELAKSANAAN_START, PELAKSANAAN_FINISH, PDF_URL, KETUA_PELAKSANA, DESKRIPSI_PROG, DANA, KETERANGAN_PESERTA FROM PROGRAM_KERJA WHERE (KODE_PROGRAM = @KODE_PROGRAM)">
            <SelectParameters>
                <asp:ControlParameter ControlID="hKodeProker" DefaultValue="hKodeProker" Name="KODE_PROGRAM" PropertyName="Value" />
            </SelectParameters>
        </asp:SqlDataSource>

              <h2>Update Status Pada Program Kerja</h2>
             <asp:GridView ID="GVUpdate" runat="server" AutoGenerateColumns="False" DataSourceID="ProkerGridUpdate">
                <Columns>
                    <asp:CommandField ShowEditButton="True" />
                    <asp:BoundField DataField="KODE_PROGRAM" HeaderText="KODE_PROGRAM" ReadOnly="True" SortExpression="KODE_PROGRAM" />
                    <asp:CheckBoxField DataField="FISIK_IS_RECEIVED" HeaderText="FISIK_IS_RECEIVED" SortExpression="FISIK_IS_RECEIVED" />
                    <asp:CheckBoxField DataField="STATUS_PROKER" HeaderText="STATUS_PROKER" SortExpression="STATUS_PROKER" />
                </Columns>
            </asp:GridView>

              <asp:SqlDataSource ID="ProkerGridUpdate" runat="server" ConflictDetection="CompareAllValues" ConnectionString="<%$ ConnectionStrings:SIKConnectionString %>" DeleteCommand="DELETE FROM [PROGRAM_KERJA] WHERE [KODE_PROGRAM] = @original_KODE_PROGRAM AND (([FISIK_IS_RECEIVED] = @original_FISIK_IS_RECEIVED) OR ([FISIK_IS_RECEIVED] IS NULL AND @original_FISIK_IS_RECEIVED IS NULL)) AND (([STATUS_PROKER] = @original_STATUS_PROKER) OR ([STATUS_PROKER] IS NULL AND @original_STATUS_PROKER IS NULL))" InsertCommand="INSERT INTO [PROGRAM_KERJA] ([KODE_PROGRAM], [FISIK_IS_RECEIVED], [STATUS_PROKER]) VALUES (@KODE_PROGRAM, @FISIK_IS_RECEIVED, @STATUS_PROKER)" OldValuesParameterFormatString="original_{0}" SelectCommand="SELECT [KODE_PROGRAM], [FISIK_IS_RECEIVED], [STATUS_PROKER] FROM [PROGRAM_KERJA] WHERE ([KODE_PROGRAM] = @KODE_PROGRAM)" UpdateCommand="UPDATE [PROGRAM_KERJA] SET [FISIK_IS_RECEIVED] = @FISIK_IS_RECEIVED, [STATUS_PROKER] = @STATUS_PROKER WHERE [KODE_PROGRAM] = @original_KODE_PROGRAM AND (([FISIK_IS_RECEIVED] = @original_FISIK_IS_RECEIVED) OR ([FISIK_IS_RECEIVED] IS NULL AND @original_FISIK_IS_RECEIVED IS NULL)) AND (([STATUS_PROKER] = @original_STATUS_PROKER) OR ([STATUS_PROKER] IS NULL AND @original_STATUS_PROKER IS NULL))">
                  <DeleteParameters>
                      <asp:Parameter Name="original_KODE_PROGRAM" Type="String" />
                      <asp:Parameter Name="original_FISIK_IS_RECEIVED" Type="Boolean" />
                      <asp:Parameter Name="original_STATUS_PROKER" Type="Boolean" />
                  </DeleteParameters>
                  <InsertParameters>
                      <asp:Parameter Name="KODE_PROGRAM" Type="String" />
                      <asp:Parameter Name="FISIK_IS_RECEIVED" Type="Boolean" />
                      <asp:Parameter Name="STATUS_PROKER" Type="Boolean" />
                  </InsertParameters>
                  <SelectParameters>
                      <asp:ControlParameter ControlID="hKodeProker" Name="KODE_PROGRAM" PropertyName="Value" Type="String" />
                  </SelectParameters>
                  <UpdateParameters>
                      <asp:Parameter Name="FISIK_IS_RECEIVED" Type="Boolean" />
                      <asp:Parameter Name="STATUS_PROKER" Type="Boolean" />
                      <asp:Parameter Name="original_KODE_PROGRAM" Type="String" />
                      <asp:Parameter Name="original_FISIK_IS_RECEIVED" Type="Boolean" />
                      <asp:Parameter Name="original_STATUS_PROKER" Type="Boolean" />
                  </UpdateParameters>
             </asp:SqlDataSource>
                   <br />

             <div class="form-group">
                <label for="exampleInputEmail1">Alasan Penolakan</label>
                <input type="text" class="form-control" id="ialasan" runat="server" aria-describedby="emailHelp" placeholder="Alasan" maxlength="50">
            </div>

             <asp:Button ID="BtUpdateAlasan" runat="server" OnClick="BtUpdateAlasan_Click" Text="Input Alasan" />

             <br />


       </div>



    
</asp:Content>
