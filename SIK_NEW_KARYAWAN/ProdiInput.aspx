﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterSite.Master" AutoEventWireup="true" CodeBehind="ProdiInput.aspx.cs" Inherits="SIK_NEW_KARYAWAN.WebForm4" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="TopPage" runat="server">


    <div class="col-md-5">
        <h1>Prodi input</h1>



        <%--        <div class='form-group'>
            <label for='NamaKabupaten'>NamaKabupaten</label>
            <select name="NamaKabupaten" id="NamaKabupaten" class="form-control">

                <option value="test"></option>
            </select>
        </div>--%>

        <asp:DropDownList ID="oFakultas" DataSourceID="SqlFakultas" DataValueField="KODE_FAKULTAS" DataTextField="NAMA_FAKULTAS"   runat="server" CssClass="form-control" AutoPostBack="true">
        </asp:DropDownList>

        <asp:SqlDataSource ID="SqlFakultas" runat="server" ConnectionString='<%$ ConnectionStrings:SIKConnectionString %>' SelectCommand="SELECT * FROM FAKULTAS"></asp:SqlDataSource>


        <%--<asp:DropDownList ID="oFakultas" runat="server">
            <asp:ListItem Enabled="true" Text="Select Month" Value="-1"></asp:ListItem>
            <asp:ListItem Text="January" Value="1"></asp:ListItem>
            <asp:ListItem Text="February" Value="2"></asp:ListItem>
        </asp:DropDownList>--%>

        <form>
            <div class="form-group">
                <label for="exampleInputnamaProdi">Nama Program Studi</label>
                <input type="text" class="form-control" maxlength="25" runat="server" id="iProdi" aria-describedby="namaprodi" placeholder="Enter Nama Prodi">
            </div>
            <asp:Button ID="btInputProdi" runat="server" CssClass="btn btn-primary" OnClick="btInputProdi_Click" Text="Submit Prodi" />
        </form>


        <asp:SqlDataSource ID="SqlProdi" runat="server" ConnectionString='<%$ ConnectionStrings:SIKConnectionString %>' SelectCommand="
            SELECT P.KODE_PRODI as KODE_PRODI, F.NAMA_FAKULTAS as NAMA_FAKULTAS , P.NAMA_PRODI as NAMA_PRODI FROM PRODI P, FAKULTAS F
WHERE P.KODE_FAKULTAS = F.KODE_FAKULTAS
            "></asp:SqlDataSource>

        <asp:GridView ID="GvProdi" runat="server" DataKeyNames="KODE_PRODI" AllowSorting="true" DataSourceID="SqlProdi" AutoGenerateColumns="true" CssClass="table">
        </asp:GridView>


    </div>

</asp:Content>
