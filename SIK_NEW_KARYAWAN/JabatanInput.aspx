﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterSite.Master" AutoEventWireup="true" CodeBehind="JabatanInput.aspx.cs" Inherits="SIK_NEW_KARYAWAN.WebForm6" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="TopPage" runat="server">


    <div class="col-md-5">
        <form>
         
            <div class="form-group">
                <label for="exampleInputEmail1">Nama Jabatan</label>
                <input runat="server" type="text" class="form-control" id="iNamaJabatan" aria-describedby="emailHelp" placeholder="Input Nama Jabatan" maxlength="50">
            </div>
            <asp:Button ID="btSubmitJabatan" runat="server" CssClass="btn btn-primary" OnClick="btSubmitJabatan_Click" Text="Submit" />

            <br />
            <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" DataSourceID="Dtjabatan">
                <Columns>
                    <asp:BoundField DataField="KODE_JABATAN" HeaderText="KODE_JABATAN" ReadOnly="True" SortExpression="KODE_JABATAN" />
                    <asp:BoundField DataField="NAMA_JABATAN" HeaderText="NAMA_JABATAN" SortExpression="NAMA_JABATAN" />
                </Columns>
            </asp:GridView>
            <asp:SqlDataSource ID="Dtjabatan" runat="server" ConnectionString="<%$ ConnectionStrings:SIKConnectionString %>" SelectCommand="SELECT [KODE_JABATAN], [NAMA_JABATAN] FROM [JABATAN]"></asp:SqlDataSource>
            <br />

        </form>
    </div>
</asp:Content>
