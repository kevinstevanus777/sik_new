﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SIK_NEW_KARYAWAN
{
    public partial class ProgramKerjaInput : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["NIK"] == null)
            {
                Response.Redirect("Login.aspx");
            }
        }

        protected void GVProgramKerjalist_SelectedIndexChanged(object sender, EventArgs e)
        {
            string ProkerID = (GVProgramKerjalist.SelectedValue).ToString();
            hKodeProker.Value = ProkerID;
            cProkerDetail.Visible = true;
            cProkerView.Visible = false;

            SIKTableAdapters.PROGRAM_KERJATableAdapter taproker = new SIKTableAdapters.PROGRAM_KERJATableAdapter();
            SIK.PROGRAM_KERJADataTable dt = taproker.SelectAlasanByKode(ProkerID);

            ialasan.Value = dt.Rows[0]["ALASAN"].ToString();

        }

       

        protected void SqlDataSource1_Selecting(object sender, SqlDataSourceSelectingEventArgs e)
        {

        }

        protected void ProkerDetail_SelectedIndexChanged(object sender, EventArgs e)
        {
         
        }

        protected void BtUpdateAlasan_Click(object sender, EventArgs e)
        {
            SIKTableAdapters.PROGRAM_KERJATableAdapter taproker = new SIKTableAdapters.PROGRAM_KERJATableAdapter();
            taproker.UpdateProkerAlasan(ialasan.Value, hKodeProker.Value);
            Response.Redirect(Request.RawUrl);
        }
    }
}