﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterSite.Master" AutoEventWireup="true" CodeBehind="FakultasInput.aspx.cs" Inherits="SIK_NEW_KARYAWAN.WebForm3" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="TopPage" runat="server">



    <div class="col-md-5">


        <%--input fakultas--%>
        <form>
            <div class="form-group">
                <h1>Input Fakultas</h1>

                <label for="exampleInputFakultas">Nama Fakultas</label>
                <input type="text" class="form-control" id="iFakultas" runat="server" aria-describedby="emailHelp" placeholder="Enter Nama Fakultas" maxlength="25">
            </div>
           
            <br />
            <asp:Button ID="btSubmitFakultas" runat="server" Text="Submit" CssClass="btn btn-primary" OnClick="btSubmitFakultas_Click" />

        </form>


        <asp:SqlDataSource ID="SqlFakultas" runat="server" ConnectionString='<%$ ConnectionStrings:SIKConnectionString %>' SelectCommand="SELECT * FROM FAKULTAS"></asp:SqlDataSource>




        <%--view fakultas--%>
        <div class="form-group" style="">


            <asp:Label ID="Label1" runat="server" Text="List Fakultas"></asp:Label>
            <asp:GridView ID="GvFakultas" runat="server" DataKeyNames="KODE_FAKULTAS" AllowSorting="false" AutoGenerateColumns="false" DataSourceID="SqlFakultas" CssClass="table">
                <Columns>
                    <asp:BoundField DataField="KODE_FAKULTAS" HeaderText="KODE FAKULTAS" />
                    <asp:BoundField DataField="NAMA_FAKULTAS" HeaderText="NAMA FAKULTAS" />
                </Columns>
            </asp:GridView>
        </div>
    </div>
</asp:Content>
