﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterSite.Master" AutoEventWireup="true" CodeBehind="LPJ_view.aspx.cs" Inherits="SIK_NEW_KARYAWAN.LPJ_view" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="TopPage" runat="server">
    <div class="col-md-5">
    
    <div id="divviewLPJ" runat="server" visible="true">
        <h1>View LPJ</h1>
        <!--nama karyawan dibawah sini -->
         <h4><asp:Label ID="LblNamaKaryawan" runat="server" Text="Nama Karyawan:"  HeaderText="Nama Karyawan"></asp:Label> </h4> 
         <asp:HiddenField ID="HKodeNIK" runat="server" />
         <asp:GridView ID="LPJGridView" runat="server" AutoGenerateColumns="False" DataSourceID="LPJListView" OnSelectedIndexChanged="LPJGridView_SelectedIndexChanged"  DataKeyNames="NO_SURAT_LPJ">
        <Columns>
             <asp:CommandField ShowSelectButton="True" />
            <asp:BoundField DataField="NO_SURAT_LPJ" HeaderText="No_surat_LPJ" ReadOnly="True" SortExpression="NO_SURAT_LPJ" />
            <asp:BoundField DataField="NOMOR_SIK" HeaderText="Nomor_SIK" SortExpression="NOMOR_SIK" />
             <asp:BoundField DataField="NIK" HeaderText="NIK" SortExpression="NIK" />
            <asp:BoundField DataField="TANGGAL_SURAT" HeaderText="Tanggal_Surat" SortExpression="TANGGAL_SURAT" />
            <asp:BoundField DataField="ALASAN" HeaderText="Alasan" SortExpression="ALASAN" />
            <asp:BoundField DataField="DANA_SISA" HeaderText="Dana_Sisa" SortExpression="DANA_SISA" />
            <asp:BoundField DataField="IS_APPROVED" HeaderText="Approved" SortExpression="IS_APPROVED" />     
        </Columns>
    </asp:GridView>
        </div>

        <asp:SqlDataSource ID="LPJListView"  ConnectionString="<%$ ConnectionStrings:SIKConnectionString %>" OnSelecting="LPJListView_Selecting" runat="server" SelectCommand="SELECT NO_SURAT_LPJ,NOMOR_SIK,NIK,TANGGAL_SURAT,ALASAN,DANA_SISA,IS_APPROVED FROM LPJ "></asp:SqlDataSource>
       
          <asp:HiddenField ID="hNoSuratLPJ" runat="server" />

    <br />

        <div id="divLPJ_update" runat="server" visible="false">
          <h2>Update Status Pada LPJ</h2>
             <asp:GridView ID="GVUpdate" runat="server" AutoGenerateColumns="False" DataSourceID="LPJGridUpdate">
                <Columns>
                    <asp:CommandField ShowEditButton="True" />
                    <asp:BoundField DataField="NO_SURAT_LPJ" HeaderText="NO_SURAT_LPJ" ReadOnly="True" SortExpression="NO_SURAT_LPJ" />
                    <asp:CheckBoxField DataField="IS_APPROVED" HeaderText="IS_APPROVED" SortExpression="IS_APPROVED" />
                </Columns>
            </asp:GridView>

        <asp:SqlDataSource ID="LPJGridUpdate" runat="server" ConnectionString="<%$ ConnectionStrings:SIKConnectionString %>" DeleteCommand="DELETE FROM [LPJ] WHERE [NO_SURAT_LPJ] = @original_NO_SURAT_LPJ AND (([IS_APPROVED] = @original_IS_APPROVED) OR ([IS_APPROVED] IS NULL AND @original_IS_APPROVED IS NULL))" InsertCommand="INSERT INTO [LPJ] ([NO_SURAT_LPJ], [IS_APPROVED]) VALUES (@NO_SURAT_LPJ, @IS_APPROVED)" SelectCommand="SELECT [NO_SURAT_LPJ], [IS_APPROVED] FROM [LPJ] WHERE ([NO_SURAT_LPJ] = @NO_SURAT_LPJ)" UpdateCommand="UPDATE [LPJ] SET [IS_APPROVED] = @IS_APPROVED WHERE [NO_SURAT_LPJ] = @original_NO_SURAT_LPJ AND (([IS_APPROVED] = @original_IS_APPROVED) OR ([IS_APPROVED] IS NULL AND @original_IS_APPROVED IS NULL))" ConflictDetection="CompareAllValues" OldValuesParameterFormatString="original_{0}">
            <DeleteParameters>
                <asp:Parameter Name="original_NO_SURAT_LPJ" Type="String" />
                <asp:Parameter Name="original_IS_APPROVED" Type="Boolean" />
            </DeleteParameters>
            <InsertParameters>
                <asp:Parameter Name="NO_SURAT_LPJ" Type="String" />
                <asp:Parameter Name="IS_APPROVED" Type="Boolean" />
            </InsertParameters>
            <SelectParameters>
                <asp:ControlParameter ControlID="hNoSuratLPJ" Name="NO_SURAT_LPJ" PropertyName="Value" Type="String" />
            </SelectParameters>
            <UpdateParameters>
                <asp:Parameter Name="IS_APPROVED" Type="Boolean" />
                <asp:Parameter Name="original_NO_SURAT_LPJ" Type="String" />
                <asp:Parameter Name="original_IS_APPROVED" Type="Boolean" />
            </UpdateParameters>
        </asp:SqlDataSource>

        <br />
    <div class="form-group">
       <label for="exampleInputEmail1">Alasan Penolakan</label>
       <input type="text" class="form-control" id="ialasan" runat="server" aria-describedby="emailHelp" placeholder="Alasan" maxlength="50">
    </div>
            <br /><br />
            <asp:Button ID="BtupdateAlasan" runat="server" Text="Update Alasan" OnClick="BtupdateAlasan_Click" />

            <br />


   </div>
</asp:Content>
