﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SIK_NEW_KARYAWAN
{
    public partial class WebForm8 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["NIK"] == null)
            {
                Response.Redirect("Login.aspx");
            }

        }

        protected void btSubmitPIC_Click(object sender, EventArgs e)
        {
            //mau ambil periode 
            //SIKTableAdapters.ORGANISASITableAdapter taorganisasi = new SIKTableAdapters.ORGANISASITableAdapter();
            //SIK.ORGANISASIDataTable dtorganisasi = taorganisasi.SelectOrganisasi



            //load table adapter PIC
            //SIKTableAdapters.PICTableAdapter tapic = new SIKTableAdapters.PICTableAdapter();
            //tapic.InsertPIC()

            //insert db
            SIKTableAdapters.PICTableAdapter tapic = new SIKTableAdapters.PICTableAdapter();
            tapic.InsertPIC(oNim.SelectedValue, oJabatan.SelectedValue, oPeriode.SelectedValue, oOrganisasi.SelectedValue);


        }

        protected void oOrganisasi_TextChanged(object sender, EventArgs e)
        {

        }
    }
}