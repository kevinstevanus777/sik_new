﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace SIK_NEW_KARYAWAN
{
    public partial class WebForm6 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if(Session["NIK"] == null)
            {
                Response.Redirect("Login.aspx");
            }
        }

        protected void btSubmitJabatan_Click(object sender, EventArgs e)
        {
            //https://stackoverflow.com/questions/51693101/how-to-create-a-custom-auto-generated-id-number-for-a-primary-key-column
           //submit ke table adapter
            SIKTableAdapters.JABATANTableAdapter tajbtn = new SIKTableAdapters.JABATANTableAdapter();
            tajbtn.InsertJabatan(iNamaJabatan.Value);
            //alert message
            ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('Data Berhasil Masuk ke DB');", true);



        }
    }
}