﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterSite.Master" AutoEventWireup="true" CodeBehind="PDF_SIK.aspx.cs" Inherits="SIK_NEW_KARYAWAN.WebForm7" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="TopPage" runat="server">
        <form>
            <h1>Input Surat Izin Kegiatan</h1>


              <asp:DropDownList ID="ddSuratProposal" DataSourceID="SqlProposal" DataValueField="NO_SURAT_PROPOSAL" DataTextField="JUDUL"   runat="server" CssClass="form-control" AutoPostBack="true">
        </asp:DropDownList>

        <asp:SqlDataSource ID="SqlProposal" runat="server" ConnectionString='<%$ ConnectionStrings:SIKConnectionString %>' SelectCommand="SELECT * FROM PROPOSAL"></asp:SqlDataSource>
            


       <div class="form-group">
            <label for="exampleInputPerihal">Perihal</label>
            <input type="text" class="form-control" id="iPerihal" runat="server" aria-describedby="emailHelp" placeholder="Input Perihal">
        </div>

        <div class="form-group">
            <label for="exampletgls">Tanggal Surat</label>
                 <br />
            <input type="text" id="tglSrt" runat="server" ClientIDMode="Static">
        </div>

    
    <asp:Button ID="BtSiksubmit" CssClass="btn btn-primary" OnClick="BtSiksubmit_Click" runat="server" Text="Submit" />
    <br />
    <br />
    <asp:Button ID="btGenerate" CssClass="btn btn-primary"  OnClick="btGenerate_Click" runat="server" Text="Generate PDF" />
    


       <!-- js -->

    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link rel="stylesheet" href="/resources/demos/style.css">
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  
    <script>

        $(function () {
            $("#tglSrt").datepicker();
     
        });
     
    </script>

    <script src="js/bootstrap.js"></script>
  
</asp:Content>



