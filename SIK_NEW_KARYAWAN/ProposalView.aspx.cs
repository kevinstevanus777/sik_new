﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using iTextSharp;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System.Diagnostics;
using System.IO;

namespace SIK_NEW_KARYAWAN
{
    public partial class ProposalView : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["NIK"] == null)
            {
                Response.Redirect("Login.aspx");
            }

        }

        protected void SqlDataSource1_Selecting(object sender, SqlDataSourceSelectingEventArgs e)
        {

        }

        protected void ProposalGridView_SelectedIndexChanged(object sender, EventArgs e)
        {
            string NoProp = (ProposalGridView.SelectedValue).ToString();
            hNoSuratProp.Value = NoProp;
            cPropDetail.Visible = true;
            cPropview.Visible = false;

            //update belum dicoba
            SIKTableAdapters.PROPOSALTableAdapter taprop = new SIKTableAdapters.PROPOSALTableAdapter();
            SIK.PROPOSALDataTable dt = taprop.SelectbyNoSurat(NoProp);

            ideksipsiisrejected.Value = dt.Rows[0]["DESKRIPSI_IS_REJECT"].ToString();
            DDStatusTempat.SelectedValue = dt.Rows[0]["STATUS_TEMPAT"].ToString(); //ini gak tau benar apa ngak
            //DDFisik_is_Recieved.SelectedValue = dt.Rows[0]["FISIK_IS_RECEIVED"].ToString();
            //DDStatus_prop.SelectedValue = dt.Rows[0]["STATUS_PROPOSAL"].ToString();

        }

        protected void BtUpdateProp_Click(object sender, EventArgs e)
        {
            //belum dicoba
            SIKTableAdapters.PROPOSALTableAdapter taproker = new SIKTableAdapters.PROPOSALTableAdapter();            
            taproker.UpdateByNoSurat(ideksipsiisrejected.Value, DDStatusTempat.SelectedValue, hNoSuratProp.Value);
            Response.Redirect(Request.RawUrl);
        }

        

        private void writeText(PdfContentByte cb, string Text, int X, int Y, BaseFont font, int Size)
        {
            cb.SetFontAndSize(font, Size);
            cb.ShowTextAligned(PdfContentByte.ALIGN_LEFT, Text, X, Y, 0);
        }
    }
}